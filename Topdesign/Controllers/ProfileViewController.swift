//
//  ProfileViewController.swift
//  Topdesign
//
//  Created by Griffith Chen on 7/1/15.
//  Copyright (c) 2015 Topdesign. All rights reserved.
//

import UIKit
import SwiftyJSON
import Haneke
import SwiftForms


class ProfileViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, CHTCollectionViewDelegateWaterfallLayout, UITableViewDelegate, UITableViewDataSource {
    
    let tabs = ["Sets", "Products", "People", "About"]
    let entities = ["set", "product", "user"]
    let emptyMessages = [
        ["No sets created yet", "No sets liked yet"],
        ["No products created yet", "No products liked yet"],
        ["You have no followers", "You are not following anyone"]
    ]
    
    var mainView: UIScrollView!
    var dashboardView: UIView!
    var tabsView: UIView!
    var tabsViewCollection: [UIView!] = []
    var tabsContentViewCollection: [UIView!] = []
    var selectedTab: Int! = 0
    var selectedTabFilter = ["created", "created", "followers"]
    
    var user: User!
    var page: Int = 0
    var model: [SwiftyJSON.JSON] = []
    
    var lastScrollOffsetY: CGFloat = 0
    var scrollDirection: ScrollDirection = .None
    var isSubmitting: Bool = false
    
    override func loadView() {
        super.loadView()
        self.view.backgroundColor = UIColor.whiteColor()
        
        let screen = UIScreen.mainScreen().bounds
        
        self.mainView = UIScrollView(frame: CGRectMake(screen.minX, screen.minY, screen.width, screen.height))
        self.mainView.contentSize = CGSizeMake(screen.width, screen.height)
        self.mainView.scrollEnabled = true
        self.mainView.showsVerticalScrollIndicator = false
        
        self.view.addSubview(self.mainView)
        
        TopDesignAPI.sharedInstance.getSession(
            { (data) -> Void in
                self.user = User(userID: data["_id"].stringValue, data: data)
                Session.sharedInstance.sync(data)
                
                self.setupNavigationBar()
                self.setupDashboard()
                self.setupTabs()
                self.setupTabViews()
                
                (self.tabsViewCollection[0] as! UIButton).sendActionsForControlEvents(.TouchUpInside)
                   
            },
            failure: { (error) -> Void in
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                let appTabBar = appDelegate.window?.rootViewController as! ApplicationTabBarController
                
                appTabBar.selectedIndex = 0
            }
        )
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.hidden = false
        self.navigationController?.navigationBar.translucent = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
        
    func setupNavigationBar() {
        let settingsButton = UIBarButtonItem(image: UIImage(named: "SettingsIcon"), style: .Plain, target: self, action: "transitionToSettings:")
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.title = "Profile"
        self.navigationItem.setRightBarButtonItem(settingsButton, animated: false)
    }
    
    func setupDashboard() {
        let screen = UIScreen.mainScreen().bounds
        let scale = UIScreen.mainScreen().scale
        let avatarSize = 24 * scale
        
        // Avatar.
        let avatar = UIImageView()
        let avatarURL = NSURL(string: self.user.avatar!)
        
        avatar.frame = CGRectMake(screen.minX + 10, screen.minY + 10, avatarSize, avatarSize)
        avatar.hnk_setImageFromURL(avatarURL!, format: Format<UIImage>(name: "original"))
        
        // Name.
        let displayName = UILabel()
        displayName.frame.offset(dx: screen.minX + 20 + avatarSize, dy: screen.minY + 10)
        displayName.text = self.user.firstName! + " " + self.user.lastName!
        displayName.font = UIFont.boldSystemFontOfSize(18)
        displayName.sizeToFit()
        
        self.dashboardView = UIView(frame: CGRectMake(screen.minX, screen.minY, screen.width, avatarSize + 20))
        self.dashboardView.addSubview(avatar)
        self.dashboardView.addSubview(displayName)
        
        self.mainView.addSubview(self.dashboardView)
    }
    
    func setupTabs() {
        let screen = UIScreen.mainScreen().bounds
        let scale = UIScreen.mainScreen().scale
        
        let tabSize = screen.width / 4
        let tabIconSize = 16 * scale
        
        var tabOffsetX : CGFloat = screen.minY
        self.tabsView = UIView(frame: CGRectMake(screen.minX, self.dashboardView.frame.maxY + 10, screen.width, tabSize))
        
        for var i = 0; i < self.tabs.count; i++ {
            let tab = UIButton()
            
            tab.frame = CGRectMake(tabOffsetX, self.dashboardView.frame.minY, tabSize, tabSize)
            tab.tag = i
            
            tab.setTitle(self.tabs[i], forState: .Normal)
            tab.setTitleColor(UIColor.blackColor(), forState: .Normal)
            tab.setImage(UIImage(named: self.tabs[i] + "Icon"), forState: .Normal)
            tab.titleLabel?.font = tab.titleLabel?.font.fontWithSize(12)
            tab.titleLabel?.sizeToFit()
            tab.addTarget(self, action: "selectTab:", forControlEvents: .TouchUpInside)
            
            let height = tabIconSize + tab.titleLabel!.frame.height
            tab.imageEdgeInsets = UIEdgeInsetsMake(-(height - tabIconSize), 0, 0, -tab.titleLabel!.frame.width + 9)
            tab.titleEdgeInsets = UIEdgeInsetsMake(0, -tabIconSize, -(height - tab.titleLabel!.frame.height), 0)
            
            tabOffsetX = tabOffsetX + tabSize
            
            let borderTopView = UIView(frame: CGRectMake(0, 0, tabSize, 1))
            let borderRightView = UIView(frame: CGRectMake(tabSize - 1, 0, 1, tabSize))
            let borderBottomView = UIView(frame: CGRectMake(0, tabSize, tabSize, 1))
            let borderLeftView = UIView(frame: CGRectMake(0, 0, 1, tabSize))
            
            borderTopView.backgroundColor = UIColor.whiteColor()
            borderRightView.backgroundColor = UIColor.whiteColor()
            borderBottomView.backgroundColor = UIColor.grayColor()
            borderLeftView.backgroundColor = UIColor.whiteColor()
            
            tab.addSubview(borderTopView)
            tab.addSubview(borderRightView)
            tab.addSubview(borderBottomView)
            tab.addSubview(borderLeftView)
            
            self.tabsViewCollection.append(tab)
            self.tabsView.addSubview(tab)
        }
        
        self.mainView.addSubview(self.tabsView)
    }
    
    func setupTabViews() {
        let screen = UIScreen.mainScreen().bounds
        let height = screen.height - self.tabsView.frame.maxY
        
        for var i = 0; i < self.tabs.count; i++ {
            let contentView = UIView(frame: CGRectMake(screen.minX, self.tabsView.frame.maxY, screen.width, height))
            contentView.hidden = true
            
            self.tabsContentViewCollection.append(contentView)
            self.mainView.addSubview(contentView)
        }
        
        self.tabsContentViewCollection[0].hidden = false
        
        setupTabContentSetsView()
        setupTabContentProductView()
        setupTabContentPeopleView()
        setupTabContentAboutView()
    }
    
    func setupTabContentSetsView() {
        let screen = UIScreen.mainScreen().bounds
        let width = screen.width / 4
        let height = CGFloat(55)
        
        let tab = self.tabsContentViewCollection[0]
        let filterView = UIView(frame: CGRectMake(screen.minX, screen.minY, screen.width, height))
        
        // "Created" filter button.
        let createdButton = UIButton(frame: CGRectMake(0, 0, width, height))
        createdButton.tag = 0
        
        createdButton.setTitle("Created", forState: .Normal)
        createdButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        createdButton.titleLabel?.font = createdButton.titleLabel?.font.fontWithSize(12)
        createdButton.titleLabel?.textAlignment = .Center
        createdButton.titleEdgeInsets = UIEdgeInsetsMake(20, 0, 0, 0)
        createdButton.addTarget(self, action: "filterTab:", forControlEvents: .TouchUpInside)
        
        let createdCountLabel = UILabel(frame: CGRectMake(0, 10, width, 20))
        createdCountLabel.text = self.user.createdSets!.stringValue
        createdCountLabel.font = UIFont.boldSystemFontOfSize(16)
        createdCountLabel.textAlignment = .Center
            
        createdButton.addSubview(createdCountLabel)
        
        // "Liked" filter button.
        let likedButton = UIButton(frame: CGRectMake(createdButton.frame.maxX, 0, width, height))
        likedButton.tag = 1
        
        likedButton.setTitle("Liked", forState: .Normal)
        likedButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        likedButton.titleLabel?.font = likedButton.titleLabel?.font.fontWithSize(12)
        likedButton.titleLabel?.textAlignment = .Center
        likedButton.titleEdgeInsets = UIEdgeInsetsMake(20, 0, 0, 0)
        likedButton.addTarget(self, action: "filterTab:", forControlEvents: .TouchUpInside)
        
        let likedCountLabel = UILabel(frame: CGRectMake(0, 10, width, 20))
        likedCountLabel.text = self.user.likedSets!.stringValue
        likedCountLabel.font = UIFont.boldSystemFontOfSize(16)
        likedCountLabel.textAlignment = .Center
            
        likedButton.addSubview(likedCountLabel)
        
        // Separator line.
        let lineView = UIView(frame: CGRectMake(0, filterView.frame.maxY, screen.width, 1))
        lineView.backgroundColor = UIColor.grayColor()
        
        // Build filterView.
        filterView.addSubview(createdButton)
        filterView.addSubview(likedButton)
        filterView.addSubview(lineView)
        
        // Empty feed message.
        let emptyFeedLabelFrame = CGRectMake(screen.minX, filterView.frame.maxY, screen.width, 30)
        let emptyFeedLabel = UILabel(frame: emptyFeedLabelFrame)
        
        emptyFeedLabel.textAlignment = .Center
        emptyFeedLabel.hidden = true

        // Feed.
        let feedSVLayout = CHTCollectionViewWaterfallLayout()
        let feedSVFrame = CGRectMake(screen.minX, filterView.frame.maxY, screen.width, 0)
        
        let feedSV = UICollectionView(frame: feedSVFrame, collectionViewLayout: feedSVLayout)
        feedSV.tag = 0
        
        feedSVLayout.minimumColumnSpacing = 5.0
        feedSVLayout.minimumInteritemSpacing = 5.0
        
        feedSV.registerNib(UINib(nibName: "ImageUICollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        feedSV.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 45, right: 0)
        
        feedSV.dataSource = self
        feedSV.delegate = self
        
        feedSV.backgroundColor = UIColor.whiteColor()
        feedSV.autoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth
        feedSV.scrollEnabled = false
        feedSV.alwaysBounceVertical = true
        feedSV.showsVerticalScrollIndicator = false
        
        // Build set tab view.
        tab.addSubview(filterView)
        tab.addSubview(emptyFeedLabel)
        tab.addSubview(feedSV)
    }
 
    func setupTabContentProductView() {
        let screen = UIScreen.mainScreen().bounds
        let width = screen.width / 4
        let height = CGFloat(55)
        
        let tab = self.tabsContentViewCollection[1]
        let filterView = UIView(frame: CGRectMake(screen.minX, screen.minY, screen.width, height))
        
        // "Added" filter button.
        let addedButton = UIButton(frame: CGRectMake(0, 0, width, height))
        addedButton.tag = 0
        
        addedButton.setTitle("Added", forState: .Normal)
        addedButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        addedButton.titleLabel?.font = addedButton.titleLabel?.font.fontWithSize(12)
        addedButton.titleLabel?.textAlignment = .Center
        addedButton.titleEdgeInsets = UIEdgeInsetsMake(20, 0, 0, 0)
        addedButton.addTarget(self, action: "filterTab:", forControlEvents: .TouchUpInside)
        
        let addedCountLabel = UILabel(frame: CGRectMake(0, 10, width, 20))
        addedCountLabel.text = self.user.createdProducts!.stringValue
        addedCountLabel.font = UIFont.boldSystemFontOfSize(16)
        addedCountLabel.textAlignment = .Center
            
        addedButton.addSubview(addedCountLabel)
        
        // "Liked" filter button.
        let likedButton = UIButton(frame: CGRectMake(addedButton.frame.maxX, 0, width, height))
        likedButton.tag = 1
        
        likedButton.setTitle("Liked", forState: .Normal)
        likedButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        likedButton.titleLabel?.font = likedButton.titleLabel?.font.fontWithSize(12)
        likedButton.titleLabel?.textAlignment = .Center
        likedButton.titleEdgeInsets = UIEdgeInsetsMake(20, 0, 0, 0)
        likedButton.addTarget(self, action: "filterTab:", forControlEvents: .TouchUpInside)
        
        let likedCountLabel = UILabel(frame: CGRectMake(0, 10, width, 20))
        likedCountLabel.text = self.user.likedProducts!.stringValue
        likedCountLabel.font = UIFont.boldSystemFontOfSize(16)
        likedCountLabel.textAlignment = .Center
            
        likedButton.addSubview(likedCountLabel)
        
        let lineView = UIView(frame: CGRectMake(0, filterView.frame.maxY, screen.width, 1))
        lineView.backgroundColor = UIColor.grayColor()
        
        filterView.addSubview(addedButton)
        filterView.addSubview(likedButton)
        filterView.addSubview(lineView)
        
        // Empty feed message.
        let emptyFeedLabelFrame = CGRectMake(screen.minX, filterView.frame.maxY, screen.width, 30)
        let emptyFeedLabel = UILabel(frame: emptyFeedLabelFrame)
        
        emptyFeedLabel.textAlignment = .Center
        emptyFeedLabel.hidden = true
        
        // Feed.
        let feedSVLayout = CHTCollectionViewWaterfallLayout()
        let feedSVFrame = CGRectMake(screen.minX, filterView.frame.maxY, screen.width, 0)
        
        let feedSV = UICollectionView(frame: feedSVFrame, collectionViewLayout: feedSVLayout)
        feedSV.tag = 0
        
        feedSVLayout.minimumColumnSpacing = 5.0
        feedSVLayout.minimumInteritemSpacing = 5.0
        
        feedSV.registerNib(UINib(nibName: "ImageUICollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        feedSV.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 45, right: 0)
        
        feedSV.dataSource = self
        feedSV.delegate = self
        
        feedSV.backgroundColor = UIColor.whiteColor()
        feedSV.autoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth
        feedSV.scrollEnabled = false
        feedSV.alwaysBounceVertical = true
        feedSV.showsVerticalScrollIndicator = false
       
        // Build product tab view.
        tab.addSubview(filterView)
        tab.addSubview(emptyFeedLabel)
        tab.addSubview(feedSV)
    }

    func setupTabContentPeopleView() {
        let screen = UIScreen.mainScreen().bounds
        let width = screen.width / 4
        let height = CGFloat(55)
        
        let tab = self.tabsContentViewCollection[2]
        let filterView = UIView(frame: CGRectMake(screen.minX, screen.minY, screen.width, height))
        
        // "Followers" filter button.
        let followersButton = UIButton(frame: CGRectMake(0, 0, width, height))
        followersButton.tag = 0
        
        followersButton.setTitle("Followers", forState: .Normal)
        followersButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        followersButton.titleLabel?.font = followersButton.titleLabel?.font.fontWithSize(12)
        followersButton.titleLabel?.textAlignment = .Center
        followersButton.titleEdgeInsets = UIEdgeInsetsMake(20, 0, 0, 0)
        followersButton.addTarget(self, action: "filterTab:", forControlEvents: .TouchUpInside)
        
        let followersCountLabel = UILabel(frame: CGRectMake(0, 10, width, 20))
        followersCountLabel.text = self.user.followers!.stringValue
        followersCountLabel.font = UIFont.boldSystemFontOfSize(16)
        followersCountLabel.textAlignment = .Center
            
        followersButton.addSubview(followersCountLabel)
        
        // "Followees" filter button.
        let followeesButton = UIButton(frame: CGRectMake(followersButton.frame.maxX, 0, width, height))
        followeesButton.tag = 1
        
        followeesButton.setTitle("Following", forState: .Normal)
        followeesButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        followeesButton.titleLabel?.font = followeesButton.titleLabel?.font.fontWithSize(12)
        followeesButton.titleLabel?.textAlignment = .Center
        followeesButton.titleEdgeInsets = UIEdgeInsetsMake(20, 0, 0, 0)
        followeesButton.addTarget(self, action: "filterTab:", forControlEvents: .TouchUpInside)
        
        let followeesCountLabel = UILabel(frame: CGRectMake(0, 10, width, 20))
        followeesCountLabel.text = self.user.followees!.stringValue
        followeesCountLabel.font = UIFont.boldSystemFontOfSize(16)
        followeesCountLabel.textAlignment = .Center
            
        followeesButton.addSubview(followeesCountLabel)
        
        let lineView = UIView(frame: CGRectMake(0, filterView.frame.maxY, screen.width, 1))
        lineView.backgroundColor = UIColor.grayColor()
        
        filterView.addSubview(followersButton)
        filterView.addSubview(followeesButton)
        filterView.addSubview(lineView)
        
        // Empty feed message.
        let emptyFeedLabelFrame = CGRectMake(screen.minX, filterView.frame.maxY, screen.width, 30)
        let emptyFeedLabel = UILabel(frame: emptyFeedLabelFrame)
        
        emptyFeedLabel.textAlignment = .Center
        emptyFeedLabel.hidden = true
       
        // Feed.
        let feedTVFrame = CGRectMake(screen.minX, filterView.frame.maxY, screen.width, 0)
        let feedTV = UITableView(frame: feedTVFrame)
        feedTV.scrollEnabled = false
        feedTV.showsVerticalScrollIndicator = false
        feedTV.hidden = true
        feedTV.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 45, right: 0)
        
        feedTV.delegate = self
        feedTV.dataSource = self
        
        // Build product tab view.
        tab.addSubview(filterView)
        tab.addSubview(emptyFeedLabel)
        tab.addSubview(feedTV)
    }
    
    func setupTabContentAboutView() {
        let screen = UIScreen.mainScreen().bounds
        let height = CGFloat(50)
        
        let tab = self.tabsContentViewCollection[3]
        
        // About.
        let aboutHeaderView = UIView(frame: CGRectMake(screen.minX, screen.minY + 1, screen.width, height))
        aboutHeaderView.backgroundColor = UIColor(hex: 0xD4D4D4)
        
        let aboutHeaderViewLabel = UILabel(frame: CGRectMake(10, 0, aboutHeaderView.frame.width, aboutHeaderView.frame.height))
        aboutHeaderViewLabel.text = "About"
        aboutHeaderViewLabel.font = UIFont.boldSystemFontOfSize(24)
        
        aboutHeaderView.addSubview(aboutHeaderViewLabel)
        
        let aboutView = UIScrollView(frame: CGRectMake(screen.minX, aboutHeaderView.frame.maxY, screen.width, height + 20))
        let aboutViewLabel = UILabel(frame: CGRectMake(10, 10, aboutView.frame.width - 10, aboutView.frame.height - 10))
        aboutViewLabel.text = self.user.about!
        aboutViewLabel.font = aboutViewLabel.font.fontWithSize(14)
        aboutViewLabel.lineBreakMode = .ByWordWrapping
        aboutViewLabel.numberOfLines = 0
        aboutViewLabel.sizeToFit()
        
        aboutView.contentSize = CGSizeMake(aboutView.frame.width, aboutViewLabel.bounds.height)
        aboutView.addSubview(aboutViewLabel)
        
        // Website.
        let websiteHeaderView = UIView(frame: CGRectMake(screen.minX, aboutView.frame.maxY, screen.width, height))
        websiteHeaderView.backgroundColor = UIColor(hex: 0xD4D4D4)
        
        let websiteHeaderViewLabel = UILabel(frame: CGRectMake(10, 0, websiteHeaderView.frame.width, websiteHeaderView.frame.height))
        websiteHeaderViewLabel.text = "Website"
        websiteHeaderViewLabel.font = UIFont.boldSystemFontOfSize(24)
            
        websiteHeaderView.addSubview(websiteHeaderViewLabel)
        
        let websiteView = UIScrollView(frame: CGRectMake(screen.minX, websiteHeaderView.frame.maxY, screen.width, height + 20))
        let websiteViewLabel = UILabel(frame: CGRectMake(10, 10, websiteView.frame.width - 10, websiteView.frame.height - 10))
        websiteViewLabel.text = self.user.website!
        websiteViewLabel.font = websiteViewLabel.font.fontWithSize(14)
        websiteViewLabel.lineBreakMode = .ByWordWrapping
        websiteViewLabel.numberOfLines = 0
        websiteViewLabel.sizeToFit()
        
        websiteView.contentSize = CGSizeMake(websiteView.frame.width, websiteViewLabel.bounds.height)
        websiteView.addSubview(websiteViewLabel)
       
        // Location.
        let locationHeaderView = UIView(frame: CGRectMake(screen.minX, websiteView.frame.maxY, screen.width, height))
        locationHeaderView.backgroundColor = UIColor(hex: 0xD4D4D4)
        
        let locationHeaderViewLabel = UILabel(frame: CGRectMake(10, 0, locationHeaderView.frame.width, locationHeaderView.frame.height))
        locationHeaderViewLabel.text = "Location"
        locationHeaderViewLabel.font = UIFont.boldSystemFontOfSize(24)
            
        locationHeaderView.addSubview(locationHeaderViewLabel)
        
        let locationView = UIScrollView(frame: CGRectMake(screen.minX, locationHeaderView.frame.maxY, screen.width, height + 20))
        let locationViewLabel = UILabel(frame: CGRectMake(10, 10, locationView.frame.width - 10, locationView.frame.height - 10))
        locationViewLabel.text = self.user.location!
        locationViewLabel.font = websiteViewLabel.font.fontWithSize(14)
        locationViewLabel.lineBreakMode = .ByWordWrapping
        locationViewLabel.numberOfLines = 0
        locationViewLabel.sizeToFit()
        
        locationView.contentSize = CGSizeMake(locationView.frame.width, locationViewLabel.bounds.height)
        locationView.addSubview(locationViewLabel)
      
        // Build about tab view.
        let aboutSC = UIScrollView(frame: CGRectMake(0, 0, tab.frame.width, tab.frame.height))
        aboutSC.addSubview(aboutHeaderView)
        aboutSC.addSubview(aboutView)
        aboutSC.addSubview(websiteHeaderView)
        aboutSC.addSubview(websiteView)
        aboutSC.addSubview(locationHeaderView)
        aboutSC.addSubview(locationView)
        
        aboutSC.contentSize = CGSizeMake(aboutSC.frame.width, locationView.frame.origin.y + locationView.frame.height)
        tab.addSubview(aboutSC)
    }
    
    func loadFeed(entity: String, state: String, page: Int, completion: (SwiftyJSON.JSON) -> Void) {
        
        if self.isSubmitting == true {
            return
        }
        
        self.isSubmitting = true
        
        TopDesignAPI.sharedInstance.getListingByEntityAndStateByUserId(self.user.userID, entity: entity, state: state) { (data) -> Void in
            
            self.page = page
            self.isSubmitting = false
            
            completion(data)
        }
    }
    
    func getStateName(tagId: Int) -> String {
        let entity = self.entities[self.selectedTab]
        var state: String = ""
        
        switch(entity) {
        case "set", "product":
            state = (tagId == 0 ? "created" : (tagId == 1 ? "liked" : ""))
        case "user":
            state = (tagId == 0 ? "followers" : (tagId == 1 ? "followees" : ""))
        default:
            state = ""
        }
        
        return state
    }
    
    func selectTab(sender: UIButton) {
        if self.tabsContentViewCollection[sender.tag] == nil {
            return
        }
        
        self.tabsContentViewCollection[self.selectedTab].hidden = true
        
        let oldTab = self.tabsViewCollection[self.selectedTab]
        let newTab = sender
        
        for var i = 1; i <= 4; i++ {
            if i == 2 {
                (oldTab.subviews[oldTab.subviews.count - i] as! UIView).backgroundColor = UIColor.grayColor()
                (newTab.subviews[newTab.subviews.count - i] as! UIView).backgroundColor = UIColor.whiteColor()
            }
            else {
                (oldTab.subviews[oldTab.subviews.count - i] as! UIView).backgroundColor = UIColor.whiteColor()
                (newTab.subviews[newTab.subviews.count - i] as! UIView).backgroundColor = UIColor.grayColor()
            }
        }
        
        self.selectedTab = sender.tag
        
        let content: UIView = self.tabsContentViewCollection[sender.tag]
        content.hidden = false
        
        if sender.tag == 0 || sender.tag == 1 {
            let contentFilter: UIView = content.subviews[0] as! UIView
            let contentFeed: UICollectionView = content.subviews[content.subviews.count - 1] as! UICollectionView
            
            if contentFeed.numberOfItemsInSection(0) == 0 {
                (contentFilter.subviews[0] as! UIButton).sendActionsForControlEvents(.TouchUpInside)
            }
        }
        else if sender.tag == 2 {
            let contentFilter: UIView = content.subviews[0] as! UIView
            let contentFeed: UITableView = content.subviews[content.subviews.count - 1] as! UITableView
            
            if contentFeed.numberOfRowsInSection(0) == 0 {
                (contentFilter.subviews[0] as! UIButton).sendActionsForControlEvents(.TouchUpInside)
            }
        }
    }
    
    func filterTab(sender: UIButton) {
        let entity = self.entities[self.selectedTab]
        let state = self.getStateName(sender.tag)
        let emptyText = self.emptyMessages[self.selectedTab][sender.tag]
        
        self.loadFeed(entity, state: state, page: 1) { (data) -> Void in
            var hits: SwiftyJSON.JSON
            var total: Int = 0
            
            let tab = self.tabsContentViewCollection[self.selectedTab]
            
            var isEmpty: Bool = false
            let tabEmptyLabel = tab.subviews[tab.subviews.count - 2] as! UILabel
            
            if entity == "user" {
                let tabTV = tab.subviews[tab.subviews.count - 1] as! UITableView
                
                hits = data["@users"]
                total = hits.count
                isEmpty = (total == 0)
                
                self.model = hits.arrayValue as [SwiftyJSON.JSON]
                
                tabTV.hidden = isEmpty
                tabTV.setContentOffset(CGPointZero, animated: false)
                tabTV.reloadData()
                
                tabTV.frame.size.height = tabTV.contentSize.height
                self.mainView.contentSize.height = tab.frame.minY + tabTV.contentSize.height + 125
            }
            else {
                let tabCV = tab.subviews[tab.subviews.count - 1] as! UICollectionView
                
                hits = data["hits"]
                total = data["total"].intValue
                isEmpty = (total == 0)
                
                self.model = hits.arrayValue as [SwiftyJSON.JSON]
                
                tabCV.hidden = isEmpty
                tabCV.setContentOffset(CGPointZero, animated: false)
                tabCV.performBatchUpdates({ tabCV.reloadSections(NSIndexSet(index: 0)) }, completion: { (finished) -> Void in
                    tabCV.frame.size.height = tabCV.contentSize.height
                    self.mainView.contentSize.height = tab.frame.minY + tabCV.contentSize.height + 125
                })
            }
            
            tabEmptyLabel.hidden = !isEmpty
            tabEmptyLabel.text = emptyText
            
            self.selectedTabFilter[self.selectedTab] = state
        }
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
  
        var cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! ImageUICollectionViewCell
        
        let data = model[indexPath.row]
        let url = NSURL(string: data["_source"]["image"]["url"].stringValue)
        
        cell.image.hnk_setImageFromURL(url!, placeholder: UIImage.imageWithColor(UIColor.lightGrayColor()))
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout!, sizeForItemAtIndexPath indexPath: NSIndexPath!) -> CGSize {
        let data = model[indexPath.row]
        let width = data["_source"]["image"]["width"].intValue
        let height = data["_source"]["image"]["height"].intValue
        let imageSize = CGSize(width: width, height: height)
        
        return imageSize
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if self.isSubmitting == true {
            return
        }
        
        let data = model[indexPath.row]
        let cell = collectionView.cellForItemAtIndexPath(indexPath)!
        
        let setViewController = SetViewController(nibName: nil, bundle: nil)
        let setId = data["_id"].stringValue
        
        self.isSubmitting = true
        
        TopDesignAPI.sharedInstance.getSetById(setId) { (data) -> Void in
            setViewController.render(setId, model: data)
            self.navigationController?.pushViewController(setViewController, animated: true)
            
            self.isSubmitting = false
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let data = self.model[indexPath.row]
        let cell = UITableViewCell(style: .Default, reuseIdentifier: nil)
        
        cell.textLabel?.text = data["user_name"].stringValue
        
        let avatar = UIImageView()
        let url = NSURL(string: data["avatar"].stringValue)
        
        avatar.hnk_setImageFromURL(url!, format: Format<UIImage>(name: "original"))
        cell.imageView?.image = avatar.image
    
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if isSubmitting == true {
            return
        }
        
        let data = self.model[indexPath.row]
        let userViewController = UserViewController(nibName: nil, bundle: nil)
        
        self.isSubmitting = true
        
        TopDesignAPI.sharedInstance.getUserById(data["_id"].stringValue) { (user) -> Void in
            self.navigationController?.pushViewController(userViewController, animated: true)
            userViewController.render(user)
            
            self.isSubmitting = false
        }
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let entity = self.entities[self.selectedTab]
        let state = self.selectedTabFilter[self.selectedTab]
        let tabView = self.tabsContentViewCollection[self.selectedTab]
        
        
        if let tableView = tabView.subviews[tabView.subviews.count - 1] as? UITableView {
            if (self.mainView.contentOffset.y + self.mainView.frame.height) > self.mainView.contentSize.height {
                self.loadFeed(entity, state: state, page: ++self.page) { (data) -> Void in
                    let hits = data["@users"].arrayValue as [SwiftyJSON.JSON]
                    
                    self.model += hits
                    tableView.reloadData()
                    
                    if hits.count > 0 {
                        self.page += 1
                    }
                }
            }
        }
        else if let scrollView = tabView.subviews[tabView.subviews.count - 1] as? UICollectionView {
            if (self.mainView.contentOffset.y + self.mainView.frame.height) > self.mainView.contentSize.height {
                self.loadFeed(entity, state: state, page: ++self.page) { (data) -> Void in
                    let hits = data["hits"].arrayValue as [SwiftyJSON.JSON]
                    
                    var index = self.model.count
                    var indexPaths = [NSIndexPath]()
                    
                    for hit in hits {
                        let indexPath = NSIndexPath(forItem: index++, inSection: 0)
                        indexPaths.append(indexPath)
                        
                        self.model.append(hit)
                    }
                    
                    scrollView.performBatchUpdates({
                            scrollView.insertItemsAtIndexPaths(indexPaths)
                        }, completion: nil)
                   
                    if hits.count > 0 {
                        self.page += 1
                    }
                }
            }
        }
    }
   
    func transitionToSettings(sender: UIBarButtonItem) {
        var settingViewController: SettingViewController!
        settingViewController = SettingViewController()
        navigationController?.pushViewController(settingViewController, animated: true)
    }
}
