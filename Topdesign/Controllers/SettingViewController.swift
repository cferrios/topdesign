//
//  SettingViewController.swift
//  Topdesign
//
//  Created by Hill Liu on 8/4/15.
//  Copyright (c) 2015 Topdesign. All rights reserved.
//

import UIKit
import SwiftForms

class SettingViewController: UITableViewController {

    
     internal override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
         let reuseIdentifier = NSStringFromClass(self.dynamicType)
         var cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: reuseIdentifier)
        }
        cell!.accessoryType = .DisclosureIndicator
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                cell!.textLabel!.text="Edit Profile"
                break
            case 1:
                cell!.textLabel!.text="Account Settings"
                break;
            default:
                break;
            }
        case 1:
            cell!.textLabel!.text="Terms and Privacy"
            break;
        default:
            break;
        }
        return cell!
        
    }

    internal convenience init() {
        self.init(style: .Grouped)
    }
    
    internal override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    internal override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            return 1
        default:
            return 0
        }
    }
    
    internal override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                var profileSettingViewController: ProfileSettingViewController!
                profileSettingViewController = ProfileSettingViewController()
                navigationController?.pushViewController(profileSettingViewController, animated: true)
                break
            case 1:
                var accountSettingViewController: AccountSettingViewController!
                accountSettingViewController = AccountSettingViewController()
                navigationController?.pushViewController(accountSettingViewController, animated: true)
                break;
            default:
                break;
            }
        case 1:
            break;
        default:
            break;
        }
    }
}