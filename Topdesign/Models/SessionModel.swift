//
//  SessionModel.swift
//  Topdesign
//
//  Created by Griffith Chen on 7/9/15.
//  Copyright (c) 2015 Topdesign. All rights reserved.
//

import Foundation
import SwiftyJSON

class Session: NSObject {
    
    let prefix: String = "TopDesignSession"
    
    var user: User?
    
    class var sharedInstance: Session {
        struct Static {
            static let instance = Session()
        }
        
        return Static.instance
    }
    
}

extension Session {
    func sync(data: SwiftyJSON.JSON) {
        let session = NSUserDefaults.standardUserDefaults()
        
        Session.sharedInstance.user = User(userID: data["_id"].stringValue, data: data)
        
        session.setValue(Session.sharedInstance.user!.userID, forKey: "\(prefix)_userID")
        session.setValue(Session.sharedInstance.user!.email, forKey: "\(prefix)_email")
    
        session.setValue(Session.sharedInstance.user!.avatar, forKey: "\(prefix)_avatar")
        session.setValue(Session.sharedInstance.user!.gender, forKey: "\(prefix)_gender")
        session.setValue(Session.sharedInstance.user!.country, forKey: "\(prefix)_country")
        session.setValue(Session.sharedInstance.user!.location, forKey: "\(prefix)_location")
        session.setValue(Session.sharedInstance.user!.firstName, forKey: "\(prefix)_firstName")
        session.setValue(Session.sharedInstance.user!.lastName, forKey: "\(prefix)_lastName")
        session.setValue(Session.sharedInstance.user!.website, forKey: "\(prefix)_website")
        session.setValue(Session.sharedInstance.user!.about, forKey: "\(prefix)_about")
        session.setValue(Session.sharedInstance.user!.userName, forKey: "\(prefix)_userName")
        session.setValue(Session.sharedInstance.user!.notificationEmail, forKey: "\(prefix)_notificationEmail")
        session.setValue(Session.sharedInstance.user!.notificationPush, forKey: "\(prefix)_notificationPush")
    
        session.setValue(Session.sharedInstance.user!.likedSets, forKey: "\(prefix)_likedSets")
        session.setValue(Session.sharedInstance.user!.createdSets, forKey: "\(prefix)_createdSets")
        session.setValue(Session.sharedInstance.user!.likedProducts, forKey: "\(prefix)_likedProducts")
        session.setValue(Session.sharedInstance.user!.createdProducts, forKey: "\(prefix)_createdProducts")
        session.setValue(Session.sharedInstance.user!.followers, forKey: "\(prefix)_followers")
        session.setValue(Session.sharedInstance.user!.followees, forKey: "\(prefix)_followees")
    
        session.synchronize()
    }
    
    func load() {
        let dict = NSUserDefaults.standardUserDefaults().dictionaryRepresentation()
        
        if dict["\(prefix)_userID"] == nil {
            return
        }
        
        Session.sharedInstance.user = User(userID: dict["\(prefix)_userID"] as! String)
        
        Session.sharedInstance.user!.email = (dict["\(prefix)_email"] as? String) ?? ""
        Session.sharedInstance.user!.avatar = (dict["\(prefix)_avatar"] as? String) ?? ""
        Session.sharedInstance.user!.gender = (dict["\(prefix)_gender"] as? String) ?? ""
        Session.sharedInstance.user!.country = (dict["\(prefix)_county"] as? String) ?? ""
        Session.sharedInstance.user!.location = (dict["\(prefix)_location"] as? String) ?? ""
        Session.sharedInstance.user!.userName = (dict["\(prefix)_userName"] as? String) ?? ""
        Session.sharedInstance.user!.firstName = (dict["\(prefix)_firstName"] as? String) ?? ""
        Session.sharedInstance.user!.lastName = (dict["\(prefix)_lastName"] as? String) ?? ""
        Session.sharedInstance.user!.website = (dict["\(prefix)_website"] as? String) ?? ""
        Session.sharedInstance.user!.about = (dict["\(prefix)_about"] as? String) ?? ""
        Session.sharedInstance.user!.notificationEmail = NSNumber(bool: dict["\(prefix)_notificationEmail"]!.boolValue) ?? 0
        Session.sharedInstance.user!.notificationPush = NSNumber(bool: dict["\(prefix)_notificationPush"]!.boolValue) ?? 0
        Session.sharedInstance.user!.likedSets = NSNumber(int: dict["\(prefix)_likedSets"]!.intValue) ?? 0
        Session.sharedInstance.user!.createdSets = NSNumber(int: dict["\(prefix)_createdSets"]!.intValue) ?? 0
        Session.sharedInstance.user!.likedProducts = NSNumber(int: dict["\(prefix)_likedProducts"]!.intValue) ?? 0
        Session.sharedInstance.user!.createdProducts = NSNumber(int: dict["\(prefix)_createdProducts"]!.intValue) ?? 0
        Session.sharedInstance.user!.followers = NSNumber(int: dict["\(prefix)_followers"]!.intValue) ?? 0
        Session.sharedInstance.user!.followees = NSNumber(int: dict["\(prefix)_followees"]!.intValue) ?? 0
    }
    
    func isSignedIn() -> Bool {
        return (Session.sharedInstance.user != nil)
    }
}