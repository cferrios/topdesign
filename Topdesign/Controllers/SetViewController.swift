//
//  SetViewController.swift
//  Topdesign
//
//  Created by Griffith Chen on 6/26/15.
//  Copyright (c) 2015 Topdesign. All rights reserved.
//

import Foundation
import Haneke
import SwiftyJSON

class SetViewController: UIViewController, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource {
    
    var mainView: UIScrollView!
    var dashboardView: UIScrollView!
    var carouselView: UIScrollView!
    var scrollboxView: UIView!
    var productView: UITableView!
    var productFeed: [SwiftyJSON.JSON] = []
    var prototypeProductCell: ProductCell!
    var backButton: UIButton!
    
    var model: Dictionary<String, SwiftyJSON.JSON>! = [:]
    var modelId: String!
    
    var isLiked: Bool! = false
    var numLikesLabel: UILabel!
    
    var isSubmitting: Bool! = false
    var isOuterScrolling: Bool! = false
    var isProductViewScrolling: Bool! = false
    
    var mainOffsetMaxY: CGFloat!
    
    override func loadView() {
        super.loadView()
        self.view.backgroundColor = UIColor.whiteColor()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func render(setId: String, model: SwiftyJSON.JSON) {
        self.modelId = setId
        
        self.mainView = UIScrollView()
        self.mainView.frame = CGRectMake(screenMinX, screenMinY, screenWidth, screenHeight)
        self.mainView.contentSize = CGSizeMake(screenWidth, screenHeight)
        self.mainView.pagingEnabled = false
        self.mainView.bounces = false
        self.mainView.alwaysBounceVertical = false
        self.mainView.showsVerticalScrollIndicator = false
        self.mainView.scrollEnabled = false
        self.mainView.delegate = self
        
        let panGesture = UIPanGestureRecognizer(target: self, action: "pan:")
        self.mainView.gestureRecognizers = [panGesture]
        
        self.view.addSubview(self.mainView)
        
        self.model["set"] = model["@set"] as SwiftyJSON.JSON
        self.model["user"] = model["@user"] as SwiftyJSON.JSON
        self.model["viewer"] = model["@viewer"] as SwiftyJSON.JSON
        
        self.renderMain()
        self.renderCarousel()
        self.renderProductFeed()
        self.renderBackButton()
        
        self.mainOffsetMaxY = self.scrollboxView.frame.minY - 20
    }
    
    func renderBackButton() {
        let backButtonImage = UIImage(named: "BackLightIcon")
        let backButtonSize = CGSizeApplyAffineTransform(backButtonImage!.size, CGAffineTransformMakeScale(0.75, 0.75))
        
        UIGraphicsBeginImageContextWithOptions(backButtonSize, true, 0.0)
        backButtonImage?.drawInRect(CGRect(origin: CGPointZero, size: backButtonSize))
            
        let backButtonScaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let backButtonWidth = backButtonScaledImage!.size.width * 2
        let backButtonHeight = backButtonScaledImage!.size.height * 2
        
        self.backButton = UIButton()
        self.backButton.frame = CGRectMake(screenMinX + 10, screenMinY + 30, backButtonWidth, backButtonHeight)
        self.backButton.backgroundColor = UIColor.blackColor()
        self.backButton.alpha = 0.7
        self.backButton.layer.cornerRadius = backButtonWidth / 2.0
        self.backButton.setImage(backButtonScaledImage, forState: .Normal)
        self.backButton.addTarget(self, action: "back:", forControlEvents: .TouchUpInside)
  
        self.mainView.addSubview(self.backButton)
    }
    
    func renderMain() {
        let imageSize = self.scaleImage(self.model["set"]!["image"])
        let height = min(imageSize.height, screenHeight - 100)
        let iconSize = 16 * screenScale
        
        // Hero image.
        let heroView = UIView(frame: CGRectMake(screenMinX, screenMinY, screenWidth, height))
        let heroImage = UIImageView(frame: CGRectMake(0, 0, heroView.frame.width, heroView.frame.height))
        let heroURL = NSURL(string: self.model["set"]!["image"]["url"].stringValue)
        heroImage.backgroundColor = UIColor.blackColor()
        heroImage.hnk_setImageFromURL(heroURL!, format: Format<UIImage>(name: "original"))
        
        // Set actions.
        let setActionLike = UIImageView(frame: CGRectMake(10, 5, iconSize, iconSize))
        let setActionLikeGR = UITapGestureRecognizer(target: self, action: Selector("likeOrUnlike:"))
        
        if Session.sharedInstance.user?.userID != nil && self.model["viewer"]!["is_liked"][self.modelId].boolValue == true {
            setActionLike.image = UIImage(named: "LikedIcon")
            self.isLiked = true
        }
        else {
            setActionLike.image = UIImage(named: "LikeIcon")
            self.isLiked = false
        }
        
        setActionLike.userInteractionEnabled = true
        setActionLike.addGestureRecognizer(setActionLikeGR)
        
        self.numLikesLabel = UILabel(frame: CGRectMake(iconSize + 15, 12, 0, 0))
        self.numLikesLabel.text = self.model["set"]!["num_likes"].stringValue
        self.numLikesLabel.textColor = UIColor.whiteColor()
        self.numLikesLabel.sizeToFit()
        
        let setActionWidth = iconSize + self.numLikesLabel.bounds.width + 25
        let setAction = UIView(frame: CGRectMake(heroView.frame.maxX - setActionWidth, 10, setActionWidth, iconSize + 10))
        setAction.backgroundColor = UIColor.blackColor()
        setAction.alpha = 0.7
        
        setAction.addSubview(setActionLike)
        setAction.addSubview(self.numLikesLabel)
        
        heroView.addSubview(heroImage)
        heroView.addSubview(setAction)
        
        // Author avatar.
        let avatar = UIImageView()
        let avatarURL = NSURL(string: self.model["user"]!["avatar"].stringValue)
        
        avatar.frame = CGRectMake(10, 0, 16 * screenScale, 16 * screenScale)
        avatar.hnk_setImageFromURL(avatarURL!, format: Format<UIImage>(name: "original"))
        
        // Author name.
        let authorName = UILabel()
        authorName.frame.offset(dx: 20 + (16 * screenScale), dy: 0)
        authorName.text = self.model["user"]!["first_name"].stringValue + " " + self.model["user"]!["last_name"].stringValue
        authorName.font = authorName.font.fontWithSize(14)
        authorName.sizeToFit()
        
        // Set created time.
        let postCreatedTime = UILabel()
        let postCreatedTimeDate = RFC3339ToDateTime(self.model["set"]!["created_time"].stringValue)
        
        postCreatedTime.frame.offset(dx: 20 + (16 * screenScale), dy: authorName.bounds.maxY + 3)
        postCreatedTime.text = "Posted " + timeAgoSinceDate(postCreatedTimeDate, false).lowercaseString
        postCreatedTime.font = postCreatedTime.font.fontWithSize(12)
        postCreatedTime.textColor = UIColor.grayColor()
        postCreatedTime.sizeToFit()
        
        let authorView = UIView()
        authorView.frame = CGRectMake(screenMinX, height + 10, screenWidth, max(avatar.bounds.height, postCreatedTime.bounds.height))
        
        authorView.userInteractionEnabled = true
        authorView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: Selector("transitionToSetAuthor:")))
        
        authorView.addSubview(avatar)
        authorView.addSubview(authorName)
        authorView.addSubview(postCreatedTime)
       
        // Set description.
        let postDescription = UILabel()
        postDescription.frame.offset(dx: screenMinX + 10, dy: authorView.frame.maxY + 10)
        postDescription.text = self.model["set"]!["description"].stringValue
        postDescription.sizeToFit()
        
        // Set tags.
        let tags = self.model["set"]!["tags"].arrayValue
        var tagButtons: [UIButton] = []
        var tagButtonsX: CGFloat = screenMinX + 10
        
        for tag in tags {
            let tagButton = UIButton()
            
            tagButton.frame = CGRectMake(tagButtonsX, postDescription.frame.maxY + 10, 0, 0)
            tagButton.backgroundColor = UIColor.grayColor()
            tagButton.setTitle(tag.stringValue, forState: UIControlState.Normal)
            tagButton.contentEdgeInsets =  UIEdgeInsetsMake(5, 9, 5, 9)
            tagButton.titleLabel?.font = tagButton.titleLabel?.font.fontWithSize(12)
            tagButton.sizeToFit()
            tagButton.addTarget(self, action: "transitionToTag:", forControlEvents: .TouchUpInside)
            
            tagButtons.append(tagButton)
            tagButtonsX += tagButton.frame.width + 5
        }
        
        // Putting it all together.
        self.dashboardView = UIScrollView(frame: CGRectMake(screenMinX, screenMinY, screenWidth, heroView.frame.maxY))
        self.dashboardView.delegate = self
        self.dashboardView.scrollEnabled = false
        self.dashboardView.bounces = false
        self.dashboardView.alwaysBounceVertical = false
        self.dashboardView.showsVerticalScrollIndicator = false
        self.dashboardView.pagingEnabled = false
        
        self.dashboardView.addSubview(heroView)
        self.dashboardView.addSubview(authorView)
        self.dashboardView.addSubview(postDescription)
        
        for tagButton in tagButtons {
            self.dashboardView.addSubview(tagButton)
        }
        
        self.dashboardView.contentSize = CGSizeMake(screenWidth, self.dashboardView.subviews[self.dashboardView.subviews.count - 1].frame.maxY)
        self.mainView.addSubview(self.dashboardView)
    }
    
    func renderCarousel() {
        let scrollboxViewHeight: CGFloat = 70.0
        let thumbnailSize: CGFloat = 48.0
        let thumbnailSpacing: CGFloat = 5.0
        
        self.scrollboxView = UIView(frame: CGRectMake(screenMinX, self.dashboardView.frame.maxY, screenWidth, scrollboxViewHeight))
        self.scrollboxView.backgroundColor = UIColor(hex: 0xF2F2F2)
        
        // Set add label button.
        let labelButtonIconSize = 25 * screenScale
        let labelButtonInset = (self.scrollboxView.frame.height - labelButtonIconSize) / 2
        
        let labelButton = UIButton(frame: CGRectMake(screenWidth - labelButtonIconSize, 0, labelButtonIconSize, scrollboxViewHeight))
        labelButton.setImage(UIImage(named: "AddTagIcon"), forState: .Normal)
        
        // Set carousel tag thumbanils.
        let labels = self.model["set"]!["labels"].arrayValue
        var labelIndex = 0
        var labelButtonsX: CGFloat = screenMinX + 10
        
        self.carouselView = UIScrollView(frame: CGRectMake(screenMinX, 0, screenWidth - labelButtonIconSize, scrollboxViewHeight))
        self.carouselView.showsHorizontalScrollIndicator = false
        self.carouselView.contentSize = CGSizeMake(10 + (CGFloat(labels.count) * (thumbnailSize + thumbnailSpacing)), scrollboxViewHeight)
        
        for label in labels {
            let labelButton = UIButton()
            let labelImageURL = NSURL(string: label["thumb_url"].stringValue)
            
            labelButton.frame = CGRectMake(labelButtonsX, 0, thumbnailSize, scrollboxViewHeight)
            labelButton.hnk_setImageFromURL(labelImageURL!, format: Format<UIImage>(name: "tagThumbnails", diskCapacity: 10 * 1024 * 1024) {
                return $0.resize(CGSizeMake(thumbnailSize, thumbnailSize)).imageWithBlackBorder()
            })
            
            labelButton.tag = labelIndex
            labelButton.addTarget(self, action: "changeTag:", forControlEvents: .TouchUpInside)
            
            let labelText = UILabel(frame: CGRectMake(labelButtonsX + (thumbnailSize - 16), labelButtonInset + 1, 16, 16))
            labelText.backgroundColor = UIColor.blackColor()
            labelText.textColor = UIColor.whiteColor()
            labelText.textAlignment = .Center
            labelText.text = String(++labelIndex)
            labelText.font = UIFont.systemFontOfSize(10)
            
            self.carouselView.addSubview(labelButton)
            self.carouselView.addSubview(labelText)
            
            labelButtonsX += labelButton.frame.width + thumbnailSpacing
        }
        
        if labelIndex > 0 {
            self.loadProductFeed(labels[0]["label_id"].stringValue, page: 1, scroll: false)
        }
        
        self.scrollboxView.addSubview(self.carouselView)
        self.scrollboxView.addSubview(labelButton)
        
        self.mainView.addSubview(self.scrollboxView)
        self.mainView.contentSize.height += scrollboxViewHeight
    }
    
    func renderProductFeed() {
        let productViewHeight: CGFloat = screenHeight - self.scrollboxView.frame.height - applicationTabBarHeight
        
        self.productView = UITableView(frame: CGRectMake(screenMinX + 10, self.scrollboxView.frame.maxY, screenWidth - 20, productViewHeight))
        self.productView.registerClass(ProductCell.self, forCellReuseIdentifier: "ProductCell")
        
        self.productView.allowsSelection = false
        self.productView.scrollEnabled = false
        self.productView.bounces = false
        self.productView.showsVerticalScrollIndicator = false
        self.productView.contentInset = UIEdgeInsetsMake(10, 0, 60, 0)
        
        self.productView.delegate = self
        self.productView.dataSource = self
        
        self.mainView.addSubview(self.productView)
        self.mainView.contentSize.height += productViewHeight
    }
    
    func scaleImage(image: SwiftyJSON.JSON) -> CGSize {
        let width = CGFloat(image["width"].floatValue)
        let height = CGFloat(image["height"].floatValue)
        
        var scale: CGFloat
        var newWidth: CGFloat = width
        var newHeight: CGFloat = height
        
        if width > height && width > screenWidth {
            scale = (screenWidth * 100) / width
            newWidth = screenWidth
            newHeight = (height * scale) / 100
        }
        
        return CGSizeMake(newWidth, newHeight)
    }
    
    func loadProductFeed(labelId: String, page: Int, scroll: Bool) {
        self.productFeed = []
        
        TopDesignAPI.sharedInstance.getProductsByLabelId(self.modelId, labelId: labelId, page: page) { (data) -> Void in
            let hits = data["hits"].arrayValue as [SwiftyJSON.JSON]
            
            self.productFeed += hits
            self.productView.reloadData()
            
            if scroll && page == 1 {
                var offset = self.scrollboxView.frame.origin
                offset.y = offset.y - 20
                
                self.mainView.setContentOffset(offset, animated: true)
                self.productView.setContentOffset(CGPointMake(0, -10), animated: false)
            }
        }
    }
    
    func transitionToTag(sender: UIButton) {
        if self.isSubmitting == true {
            return
        }
        
        let tagViewController = TagViewController(nibName: nil, bundle: nil)
        let tag = sender.titleLabel!.text!
        
        tagViewController.title = tag
        self.isSubmitting = true
            
        TopDesignAPI.sharedInstance.getListingByTag(tag, page: 1) { (data) -> Void in
            self.navigationController?.pushViewController(tagViewController, animated: true)
            
            tagViewController.tag = tag
            tagViewController.model = data["hits"].arrayValue as [SwiftyJSON.JSON]
            
            self.isSubmitting = false
        }
    }
    
    func transitionToSetAuthor(sender: UIGestureRecognizer) {
        if self.isSubmitting == true {
            return
        }
        
        let userViewController = UserViewController(nibName: nil, bundle: nil)
        self.isSubmitting = true
        
        TopDesignAPI.sharedInstance.getUserById(self.model["user"]!["_id"].stringValue) { (data) -> Void in
            self.navigationController?.pushViewController(userViewController, animated: true)
            userViewController.render(data)
            
            self.isSubmitting = false
        }
    }
    
    func transitionToProductAuthor(sender: UIGestureRecognizer) {
        let data = self.productFeed[sender.view!.tag]

        if data == nil || self.isSubmitting == true {
            return
        }
        
        let userViewController = UserViewController(nibName: nil, bundle: nil)
        self.isSubmitting = true
        
        TopDesignAPI.sharedInstance.getUserById(data["@user"]["_id"].stringValue) { (data) -> Void in
            self.navigationController?.pushViewController(userViewController, animated: true)
            userViewController.render(data)
            
            self.isSubmitting = false
        }
    }
    
    func transitionToStore(sender: UIButton) {
        if let data = self.productFeed[sender.tag] as SwiftyJSON.JSON? {
            UIApplication.sharedApplication().openURL(NSURL(string: data["_source"]["site_url"].stringValue)!)
        }
    }
   
    func likeOrUnlike(sender: UIGestureRecognizer) {
        self.likeOrUnlike(sender.view as! UIImageView, isLike: !self.isLiked)
    }
    
    func likeOrUnlike(sender: UIImageView, isLike: Bool) {
        if self.isSubmitting == true {
            return
        }
        
        self.isSubmitting = true
        
        TopDesignAPI.sharedInstance.likeOrUnlikeById("set", entityId: self.modelId, isLike: isLike,
            success: { (data) -> Void in
                if isLike == true {
                    self.model["set"]!["num_likes"].intValue += 1
                    sender.image = UIImage(named: "LikedIcon")
                }
                else {
                    self.model["set"]!["num_likes"].intValue -= 1
                    sender.image = UIImage(named: "LikeIcon")
                }
                
                self.isLiked = isLike
                self.numLikesLabel.text = self.model["set"]!["num_likes"].stringValue
                
                self.isSubmitting = false
            },
            failure: { (error) -> Void in
                if error.code == 401 {
                    let loginVC = LoginViewController()
                    loginVC.modalPresentationStyle = .OverFullScreen
        
                    self.presentViewController(loginVC, animated: false, completion: nil)
                }
                else {
                    self.error("System Error", message: error.localizedDescription)
                }
                
                self.isSubmitting = false
            }
        )
    }
    
    func changeTag(sender: UIButton) {
        if let label = self.model["set"]?["labels"][sender.tag] {
            self.loadProductFeed(label["label_id"].stringValue, page: 1, scroll: true)
        }
    }
    
    func error(title: String, message: String) {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alertView.addAction(UIAlertAction(title: "Dismiss", style: .Default, handler: nil))
        
        self.presentViewController(alertView, animated: true, completion: nil)
    }

    func back(sender: UIBarButtonItem) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.productFeed.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("ProductCell", forIndexPath: indexPath) as! ProductCell
        self.configureCell(cell, atIndexPath: indexPath)
        
        cell.productStoreButton.addTarget(self, action: "transitionToStore:", forControlEvents: .TouchUpInside)
        
        let authorViewGR = UITapGestureRecognizer(target: self, action: Selector("transitionToProductAuthor:"))
        cell.authorView.userInteractionEnabled = true
        cell.authorView.addGestureRecognizer(authorViewGR)
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if self.prototypeProductCell == nil {
            self.prototypeProductCell = tableView.dequeueReusableCellWithIdentifier("ProductCell") as! ProductCell
        }
        
        self.configureCell(prototypeProductCell, atIndexPath: indexPath)
        return prototypeProductCell.authorView.frame.maxY
    }
    
    func configureCell(cell: ProductCell, atIndexPath: NSIndexPath) {
        let data = self.productFeed[atIndexPath.row]
        let url = NSURL(string: data["_source"]["image"]["url"].stringValue)
        let thumbSize = self.scaleImage(data["_source"]["image"])
        
        cell.thumbnail.frame.size = thumbSize
        cell.thumbnail.center = CGPoint(x: cell.center.x, y: thumbSize.height / 2)
        cell.thumbnail.hnk_setImageFromURL(url!, placeholder: UIImage.imageWithColor(UIColor.lightGrayColor()))
        
        let buttonTitle = data["_source"]["price"].intValue > 0 ? "$" + data["_source"]["price"].stringValue : "Visit Store"
        cell.productStoreButton.frame = CGRectMake(cell.frame.width - 100, 10, 90, 25)
        cell.productStoreButton.setTitle(buttonTitle, forState: .Normal)
        cell.productStoreButton.tag = atIndexPath.row
        
        cell.productNameLabel.frame = CGRectMake(10, 10, cell.frame.width - (cell.productStoreButton.frame.width + 20), 0)
        cell.productNameLabel.text = data["_source"]["product_name"].stringValue
        cell.productNameLabel.sizeToFit()
        
        cell.productView.frame = CGRectMake(0, cell.thumbnail.frame.maxY, cell.frame.width, max(cell.productStoreButton.frame.maxY, cell.productNameLabel.frame.maxY))
        cell.productView.tag = atIndexPath.row
        
        let avatarURL = NSURL(string: data["@user"]["avatar"].stringValue)
        cell.authorAvatar.frame = CGRectMake(10, 10, 36, 36)
        cell.authorAvatar.hnk_setImageFromURL(avatarURL!, placeholder: UIImage.imageWithColor(UIColor.lightGrayColor()), format: Format<UIImage>(name: "original"))
        
        cell.authorName.frame = CGRectMake(cell.authorAvatar.frame.maxX + 10, 10, 0, 0)
        cell.authorName.text = data["@user"]["first_name"].stringValue + " " + data["@user"]["last_name"].stringValue
        cell.authorName.sizeToFit()
        
        let postCreatedTimeDate = RFC3339ToDateTime(data["_source"]["created_time"].stringValue)
        cell.postDate.frame = CGRectMake(cell.authorAvatar.frame.maxX + 10, cell.authorName.frame.maxY, 0, 0)
        cell.postDate.text = "Posted " + timeAgoSinceDate(postCreatedTimeDate, false).lowercaseString
        cell.postDate.sizeToFit()
        
        cell.authorView.frame = CGRectMake(0, cell.productView.frame.maxY + 10, cell.frame.width, max(cell.authorAvatar.frame.maxY, cell.postDate.frame.maxY) + 10)
        cell.authorView.tag = atIndexPath.row
    }
    
    func pan(sender: UIPanGestureRecognizer) {
        let translation = sender.translationInView(self.mainView)
        let velocity = sender.velocityInView(self.mainView)
        let mainOffset = self.mainView.contentOffset
        
        if sender.state == .Changed {
            var adjustedOffset: CGFloat = 0
            
            let dashboardOffset = self.dashboardView.contentOffset
            let dashboardOffsetMaxY = self.dashboardView.contentSize.height - self.dashboardView.frame.height
            
            let productViewOffset = self.productView.contentOffset
            let productViewOffsetMaxY = max(-10, self.productView.contentSize.height - (self.productView.frame.height - 60))
            
            if (self.isOuterScrolling == true && ceil(mainOffset.y) < 0) || (ceil(dashboardOffset.y) < dashboardOffsetMaxY){
                adjustedOffset = min(max(0, dashboardOffset.y - translation.y), dashboardOffsetMaxY)
                self.dashboardView.contentOffset = CGPointMake(dashboardOffset.x, adjustedOffset)
                self.isOuterScrolling = false
            }
            else {
                adjustedOffset = min(max(-statusBarHeight, mainOffset.y - translation.y), self.mainOffsetMaxY)
                
                if self.isProductViewScrolling == false && productViewOffset.y < 0 {
                    self.mainView.contentOffset = CGPointMake(mainOffset.x, adjustedOffset)
                    self.isOuterScrolling = true
                    
                    if adjustedOffset == self.mainOffsetMaxY {
                        self.isProductViewScrolling = true
                    }
                }
                else {
                    self.productView.contentOffset = CGPointMake(mainOffset.x, min(max(-10, productViewOffset.y - translation.y), productViewOffsetMaxY))
                    self.isOuterScrolling = false
                    
                    if productViewOffset.y < 0 {
                        self.isProductViewScrolling = false
                    }
                }
            }
        }
        else if sender.state == .Ended {
            let magnitude = sqrt(velocity.y * velocity.y)
            let multiplier = magnitude / 100.0
            let factor = 0.1 * multiplier
            
            let productViewOffset = self.productView.contentOffset
            var finalOffsetY = min(max(0, mainOffset.y - (velocity.y * factor)), self.mainOffsetMaxY)
            
            if self.isProductViewScrolling == false && productViewOffset.y < 0 {
                UIView.animateWithDuration(NSTimeInterval(min(0.5, factor * 1.2)), delay: 0.0, options: .CurveEaseOut | .AllowUserInteraction, animations: {
                    self.mainView.contentOffset = CGPointMake(mainOffset.x, finalOffsetY)
                }, completion: nil)
            }
            else {
                UIView.animateWithDuration(NSTimeInterval(min(0.5, factor * 1.2)), delay: 0.0, options: .CurveEaseOut | .AllowUserInteraction, animations: {
                    self.productView.contentOffset = CGPointMake(mainOffset.x, finalOffsetY)
                }, completion: nil)
            }
        }
        
        sender.setTranslation(CGPointZero, inView: self.mainView)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let offsetY = min(self.mainView.contentOffset.y + 30, self.scrollboxView.frame.minY - (self.backButton.frame.height + 10))
        self.backButton.frame.origin = CGPointMake(10, offsetY)
    }
}