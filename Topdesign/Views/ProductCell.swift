//
//  ProductCell.swift
//  Topdesign
//
//  Created by Griffith Chen on 8/21/15.
//  Copyright (c) 2015 Topdesign. All rights reserved.
//

import Foundation

class ProductCell: UITableViewCell {
    
    var thumbnail = UIImageView()
    
    var productView = UIView()
    var productNameLabel = UILabel()
    var productStoreButton = UIButton()
    
    var tagView = UIView()
    
    var authorView = UIView()
    var authorAvatar = UIImageView()
    var authorName = UILabel()
    var postDate = UILabel()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 6
        
        let pLineView = UIView(frame: CGRectMake(0, 0, self.contentView.frame.width, 1))
        pLineView.backgroundColor = UIColor.lightGrayColor()
        self.productView.addSubview(pLineView)
        
        self.productNameLabel.font = UIFont.boldSystemFontOfSize(14)
        self.productNameLabel.lineBreakMode = .ByWordWrapping
        self.productNameLabel.numberOfLines = 0
        self.productView.addSubview(self.productNameLabel)
        
        self.productStoreButton.backgroundColor = UIColor(hex: 0x1588fb)
        self.productStoreButton.titleLabel!.font = UIFont.systemFontOfSize(12)
        self.productStoreButton.layer.cornerRadius = 3
        self.productView.addSubview(self.productStoreButton)
        
        let aLineView = UIView(frame: CGRectMake(0, 0, self.contentView.frame.width, 1))
        aLineView.backgroundColor = UIColor.lightGrayColor()
        
        self.authorView.addSubview(aLineView)
        self.authorView.addSubview(self.authorAvatar)
        
        self.authorName.font = UIFont.systemFontOfSize(14)
        self.authorView.addSubview(self.authorName)
        
        self.postDate.textColor = UIColor.grayColor()
        self.postDate.font = UIFont.systemFontOfSize(12)
        self.authorView.addSubview(self.postDate)
        
        self.contentView.addSubview(self.thumbnail)
        self.contentView.addSubview(self.productView)
        self.contentView.addSubview(self.authorView)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
