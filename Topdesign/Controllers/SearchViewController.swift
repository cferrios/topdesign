//
//  SearchViewController.swift
//  Topdesign
//
//  Created by Griffith Chen on 6/3/15.
//  Copyright (c) 2015 Topdesign. All rights reserved.
//

import UIKit
import SwiftyJSON
import Haneke

class SearchViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
    
    let searchSections = ["Tags", "People"]
    let searchSectionKeys = ["tag", "user"]

    var searchDebounced: (() -> ())!
    var searchBar: UISearchBar!
    var searchTableView: UITableView!
    
    var searchText = ""
    var searchFilter = ""
    var searchResult: [SwiftyJSON.JSON] = []
    
    var isSubmitting: Bool = false
    
    override func loadView() {
        super.loadView()
        
        self.view.backgroundColor = UIColor.whiteColor()
        
        setupSearchTableView()
        setupSearchBar()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupSearchBar() {
        let f = UIScreen.mainScreen().bounds
        let bgColor = UIImage.imageWithColor(UIColor.whiteColor())
        
        self.searchBar = UISearchBar()
        self.searchBar.frame = CGRectMake(f.minX + 10, f.minY + 30, f.width - 20, f.height * 0.05)
        self.searchBar.searchBarStyle = UISearchBarStyle.Minimal
        
        self.searchBar.showsScopeBar = true
        self.searchBar.scopeButtonTitles = self.searchSections
        self.searchBar.scopeBarBackgroundImage = bgColor
        
        self.searchBar.delegate = self
        self.view.addSubview(self.searchBar)
        
        self.searchFilter = self.searchSectionKeys[0]
        self.searchDebounced = debounce(NSTimeInterval(0.25), queue: dispatch_get_main_queue(), self.search)
    }
    
    func setupSearchTableView() {
        let f = UIScreen.mainScreen().bounds
        
        self.searchTableView = UITableView()
        self.searchTableView.frame = CGRectMake(0, f.minY + 100, f.width, f.height)
        
        self.searchTableView.delegate = self
        self.searchTableView.dataSource = self
        
        self.searchTableView.scrollEnabled = true
        self.searchTableView.hidden = true
        
        self.searchTableView.tableHeaderView = self.searchBar
        self.view.addSubview(self.searchTableView)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: Selector("hideKeyboard"))
        tapRecognizer.cancelsTouchesInView = false
        
        self.searchTableView.addGestureRecognizer(tapRecognizer)
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchText = searchText
        searchDebounced()
        
        if searchText.isEmpty {
            searchBar.endEditing(true)
        }
    }
    
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        self.searchFilter = self.searchSectionKeys[selectedScope]
        searchDebounced()
    }
    
    func search() {
        if self.searchText == "" {
            self.searchResult = []
            
            self.searchTableView.hidden = true
            self.searchTableView.reloadData()
            
            return
        }
        
        TopDesignAPI.sharedInstance.search(self.searchText, type: self.searchFilter) { (data) -> Void in
            var tags = data["@tags"].arrayValue as [SwiftyJSON.JSON]
            var users = data["@users"].arrayValue as [SwiftyJSON.JSON]
            
            if tags.count + users.count == 0 {
                self.searchTableView.hidden = true
                return
            }
            
            self.searchResult = tags + users
            self.searchTableView.hidden = false
            
            self.searchTableView.reloadData()
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchResult.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let data = self.searchResult[indexPath.row]
        let cell = UITableViewCell(style: .Default, reuseIdentifier: nil)
        
        if self.searchFilter == "tag" {
            cell.textLabel?.text = data.stringValue
        }
        else {
            cell.textLabel?.text = data["user_name"].stringValue
            
            let avatar = UIImageView()
            let url = NSURL(string: data["avatar"].stringValue)
            
            avatar.hnk_setImageFromURL(url!, format: Format<UIImage>(name: "original"))
            cell.imageView?.image = avatar.image
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let data = self.searchResult[indexPath.row]
        
        if self.isSubmitting == true {
            return
        }
        
        if self.searchFilter == "tag" {
            let tagViewController = TagViewController(nibName: nil, bundle: nil)
            let tag = data.stringValue
            
            tagViewController.title = tag
            self.isSubmitting = true
            
            TopDesignAPI.sharedInstance.getListingByTag(tag, page: 1) { (data) -> Void in
                self.navigationController?.pushViewController(tagViewController, animated: true)
                
                tagViewController.tag = tag
                tagViewController.model = data["hits"].arrayValue as [SwiftyJSON.JSON]
                
                self.isSubmitting = false
            }
        }
        else if self.searchFilter == "user" {
            let userViewController = UserViewController(nibName: nil, bundle: nil)
            let userId = data["_id"].stringValue
            
            self.isSubmitting = true
        
            TopDesignAPI.sharedInstance.getUserById(userId) { (data) -> Void in
                self.navigationController?.pushViewController(userViewController, animated: true)
                userViewController.render(data)
            
                self.isSubmitting = false
            }
        }
    }
    
    func hideKeyboard() {
        self.searchBar.resignFirstResponder()
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        self.searchBar.resignFirstResponder()
    }
}