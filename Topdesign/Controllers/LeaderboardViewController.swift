//
//  LeaderboardViewController.swift
//  Topdesign
//
//  Created by Griffith Chen on 8/1/15.
//  Copyright (c) 2015 Topdesign. All rights reserved.
//

import Foundation

class LeaderboardViewController: UIViewController {
    
    override func loadView() {
        super.loadView()
        self.view.backgroundColor = UIColor.lightGrayColor()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = true
    }
    
}