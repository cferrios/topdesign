//
//  MainTabBarController.swift
//  Topdesign
//
//  Created by Griffith Chen on 7/7/15.
//  Copyright (c) 2015 Topdesign. All rights reserved.
//

import UIKit
import MobileCoreServices

class ApplicationTabBarController: UITabBarController, UITabBarControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var postButton: UIButton!
    var actionSheet: UIAlertController!
    
    override func loadView() {
        super.loadView()
        
        setupActionSheet()
        setupItems()
    }
    
    func setupActionSheet() {
        self.actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        self.actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .Default, handler: { (action) in
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .PhotoLibrary
            
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }))
        
        self.actionSheet.addAction(UIAlertAction(title: "Take Photo", style: .Default, handler: { (action) in
            if !UIImagePickerController.isSourceTypeAvailable(.Camera) {
                self.error("Error", message: "Cannot connect to camera")
                return
            }
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .Camera
            imagePicker.mediaTypes = [kUTTypeImage!]
            
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }))
       
        self.actionSheet.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
    }
    
    func setupItems() {
        // Browse.
        let browseView = BrowseViewController(nibName: nil, bundle: nil)
        let browseNav = UINavigationController(nibName: nil, bundle: nil)
        
        browseView.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: "BrowseIcon"), selectedImage: UIImage(named: "BrowseSelectedIcon"))
        browseView.tabBarItem.imageInsets = UIEdgeInsetsMake(5.5, 0, -5.5, 0)
        browseNav.addChildViewController(browseView)
        
        // Search.
        let searchView = SearchViewController(nibName: nil, bundle: nil)
        let searchNav = UINavigationController(nibName: nil, bundle: nil)
        
        searchView.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: "SearchIcon"), selectedImage: UIImage(named: "SearchSelectedIcon"))
        searchView.tabBarItem.imageInsets = UIEdgeInsetsMake(5.5, 0, -5.5, 0)
        searchNav.addChildViewController(searchView)
        
        // Post
        let postNav = UINavigationController(nibName: nil, bundle: nil)
        
        // Leaderboard.
        let leaderboardView = LeaderboardViewController(nibName: nil, bundle: nil)
        let leaderboardNav = UINavigationController(nibName: nil, bundle: nil)
        
        leaderboardView.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: "LeaderboardIcon"), selectedImage: UIImage(named: "LeaderboardSelectedIcon"))
        leaderboardView.tabBarItem.imageInsets = UIEdgeInsetsMake(5.5, 0, -5.5, 0)
        leaderboardNav.addChildViewController(leaderboardView)
        
        // Profile.
        let profileView = ProfileViewController(nibName: nil, bundle: nil)
        let profileNav = UINavigationController(nibName: nil, bundle: nil)
        
        profileView.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: "ProfileIcon"), selectedImage: UIImage(named: "ProfileSelectedIcon"))
        profileView.tabBarItem.imageInsets = UIEdgeInsetsMake(5.5, 0, -5.5, 0)
        profileNav.addChildViewController(profileView)
        
        // Bootstrap the TabBar center button.
        let postImage = UIImage(named: "PostIcon")!
        self.postButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        
        self.postButton.frame = CGRectMake(0, 0, 30, 30)
        self.postButton.layer.cornerRadius =  0.79 * postButton.bounds.size.width
        self.postButton.backgroundColor = UIColor.redColor()
        self.postButton.setTranslatesAutoresizingMaskIntoConstraints(false)
        self.postButton.setImage(postImage, forState: .Normal)
        self.postButton.setImage(postImage, forState: .Highlighted)
        self.postButton.addTarget(self, action: "post:", forControlEvents: .TouchUpInside)
        
        self.view.addSubview(self.postButton)
        
        self.view.addConstraint(NSLayoutConstraint(item: self.postButton, attribute: .Height, relatedBy: .Equal, toItem: self.tabBar, attribute: .Height, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: self.postButton, attribute: .Width, relatedBy: .Equal, toItem: self.tabBar, attribute: .Height, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: self.postButton, attribute: .CenterX, relatedBy: .Equal, toItem: self.view, attribute: .CenterX, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: self.postButton, attribute: .CenterY, relatedBy: .Equal, toItem: self.tabBar, attribute: .CenterY, multiplier: 1, constant: -11))
    
        self.delegate = self
        self.viewControllers = [browseNav, searchNav, postNav, leaderboardNav, profileNav]
    }
    
    func tabBarController(tabBarController: UITabBarController, shouldSelectViewController viewController: UIViewController) -> Bool {
        let tabItems = tabBarController.viewControllers! as NSArray
        var shouldSelect: Bool = true
        
        if tabItems.indexOfObject(viewController) == 2 {
            return false
        }
        
        if tabItems.indexOfObject(viewController) == 4 && !Session.sharedInstance.isSignedIn() {
            shouldSelect = false
            
            let loginVC = LoginViewController()
            loginVC.modalPresentationStyle = .OverFullScreen
            
            tabBarController.selectedViewController?.presentViewController(loginVC, animated: false, completion: nil)
        }
        
        return shouldSelect
    }

    func toggleVisibility(visible: Bool, animated: Bool) {
        if isVisible() == visible {
            return
        }
        
        let frame = self.tabBar.frame
        let height = frame.height + self.postButton.frame.height
        let offsetY = visible ? -height : height
        let duration = animated ? 0.3 : 0.0
        
        self.tabBar.frame = CGRectOffset(frame, 0, offsetY)
    }
    
    func isVisible() -> Bool {
        return self.tabBar.frame.origin.y < CGRectGetMaxY(self.view.frame)
    }
    
    func post(sender: UIButton!) {
        if !Session.sharedInstance.isSignedIn() {
            let loginVC = LoginViewController()
            loginVC.modalPresentationStyle = .OverFullScreen
            
            self.selectedViewController?.presentViewController(loginVC, animated: false, completion: nil)
            return
        }
        
        self.presentViewController(self.actionSheet, animated: true, completion: nil)
    }
    
    func error(title: String, message: String) {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alertView.addAction(UIAlertAction(title: "Dismiss", style: .Default, handler: nil))
        
        self.presentViewController(alertView, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        self.dismissViewControllerAnimated(true, completion: nil)
        
        let addSetVC = AddSetViewController()
        self.selectedViewController?.presentViewController(addSetVC, animated: true, completion: {
            let photo = UIImageView()
            photo.image = info[UIImagePickerControllerOriginalImage] as? UIImage
            
            addSetVC.setupPhoto(photo)
        })
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}