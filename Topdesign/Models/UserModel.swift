//
//  UserModel.swift
//  Topdesign
//
//  Created by Griffith Chen on 7/23/15.
//  Copyright (c) 2015 Topdesign. All rights reserved.
//

import Foundation
import SwiftyJSON

struct User {
    
    var userID: String
    var email: String? = ""
    var avatar: String? = ""
    var gender: String? = ""
    var country: String? = ""
    var location: String? = ""
    var userName: String? = ""
    var firstName: String? = ""
    var lastName: String? = ""
    var website: String? = ""
    var about: String? = ""
    var notificationEmail: NSNumber? = 1
    var notificationPush: NSNumber? = 1
    var likedSets: NSNumber? = 0
    var createdSets: NSNumber? = 0
    var likedProducts: NSNumber? = 0
    var createdProducts: NSNumber? = 0
    var followers: NSNumber? = 0
    var followees: NSNumber? = 0
    
    init(userID: String) {
        self.userID = userID
    }
    
    init(userID: String, data: SwiftyJSON.JSON) {
        self.userID = userID
        
        if data["_source"]["account"] != nil {
            self.email = data["_source"]["account"]["email"].stringValue
        }
        
        if data["_source"]["profile"] != nil {
            self.avatar = data["_source"]["profile"]["avatar"].stringValue
            self.gender = data["_source"]["profile"]["gender"].stringValue
            self.country = data["_source"]["profile"]["country"].stringValue
            self.location = data["_source"]["profile"]["location"].stringValue
            self.firstName = data["_source"]["profile"]["first_name"].stringValue
            self.lastName = data["_source"]["profile"]["last_name"].stringValue
            self.website = data["_source"]["profile"]["website"].stringValue
            self.about = data["_source"]["profile"]["about"].stringValue
            self.userName = data["_source"]["profile"]["user_name"].stringValue
            self.notificationEmail = data["_source"]["profile"]["notification_email"].boolValue
            self.notificationPush = data["_source"]["profile"]["notification_push"].boolValue
        }
        
        if data["_source"]["stats"] != nil {
            self.likedSets = data["_source"]["stats"]["liked_sets"].intValue
            self.createdSets = data["_source"]["stats"]["created_sets"].intValue
            self.likedProducts = data["_source"]["stats"]["liked_products"].intValue
            self.createdProducts = data["_source"]["stats"]["created_products"].intValue
            self.followers = data["_source"]["stats"]["followers"].intValue
            self.followees = data["_source"]["stats"]["followees"].intValue
        }
    }
}