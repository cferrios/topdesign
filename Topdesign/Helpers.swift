//
//  Helpers.swift
//  Topdesign
//
//  Created by Griffith Chen on 6/16/15.
//  Copyright (c) 2015 Topdesign. All rights reserved.
//

import Foundation
import UIKit

let screenBounds = UIScreen.mainScreen().bounds
let screenScale = UIScreen.mainScreen().scale
let screenMinX = screenBounds.minX
let screenMinY = screenBounds.minY
let screenSize = screenBounds.size
let screenWidth = screenSize.width
let screenHeight = screenSize.height

let navBarHeight: CGFloat = 44.0
let statusBarHeight: CGFloat = 20.0
let applicationTabBarHeight: CGFloat = 56.0

enum ScrollDirection: Int {
    case None = 0
    case Up = 1
    case Right = 2
    case Down = 3
    case Left = 4
}
       
extension UIImage {
    class func imageWithColor(color: UIColor) -> UIImage {
        let rect: CGRect = CGRectMake(0, 0, 1, 1)
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(1, 1), false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    func imageWithBlackBorder() -> UIImage {
        let imageSize: CGSize = self.size
        let rect: CGRect = CGRectMake(0, 0, imageSize.width, imageSize.height)
        
        UIGraphicsBeginImageContext(imageSize)
        self.drawInRect(rect, blendMode: kCGBlendModeNormal, alpha: 1.0)
        
        let context: CGContextRef = UIGraphicsGetCurrentContext()
        CGContextSetRGBStrokeColor(context, 0, 0, 0, 1)
        CGContextStrokeRect(context, rect)
        
        let resultImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return resultImage
    }
    
    func resize(sizeChange: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(sizeChange, false, 0)
        self.drawInRect(CGRect(origin: CGPointZero, size: sizeChange))
        
        let scaledImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return scaledImage
    }
}

extension UIColor {
    convenience init(hex: Int) {
        self.init(hex: hex, alpha: 1)
    }
    
    convenience init(hex: Int, alpha: Double) {
        self.init(
            red: CGFloat((hex >> 16) & 0xff) / 255,
            green: CGFloat((hex >> 8) & 0xff) / 255,
            blue: CGFloat(hex & 0xff) / 255,
            alpha: CGFloat(alpha))
    }
}

func toggleApplicationTabBar(visible: Bool, animated: Bool) {
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let tabBar = appDelegate.window!.rootViewController as! ApplicationTabBarController
    
    tabBar.toggleVisibility(visible, animated: animated)
}

func debounce(delay: NSTimeInterval, #queue: dispatch_queue_t, action: (() -> ())) -> () -> () {
    var lastFireTime: dispatch_time_t = 0
    let dispatchDelay = Int64(delay * Double(NSEC_PER_SEC))
    
    return {
        lastFireTime = dispatch_time(DISPATCH_TIME_NOW,0)
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                dispatchDelay
            ),
            queue) {
                let now = dispatch_time(DISPATCH_TIME_NOW,0)
                let when = dispatch_time(lastFireTime, dispatchDelay)
                if now >= when {
                    action()
                }
        }
    }
}

func RFC3339ToDateTime(dateString: NSString) -> NSDate {
    let formatter = NSDateFormatter()
    formatter.locale = NSLocale(localeIdentifier: "en_US")
    formatter.timeZone = NSTimeZone(name: "UTC")
    formatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'"
    
    let result = formatter.dateFromString(dateString as String)
    return result!
}

func timeAgoSinceDate(date: NSDate, numericDates: Bool) -> String {
    let calendar = NSCalendar.currentCalendar()
    let unitFlags = NSCalendarUnit.CalendarUnitMinute | NSCalendarUnit.CalendarUnitHour | NSCalendarUnit.CalendarUnitDay | NSCalendarUnit.CalendarUnitWeekOfYear | NSCalendarUnit.CalendarUnitMonth | NSCalendarUnit.CalendarUnitYear | NSCalendarUnit.CalendarUnitSecond
    let now = NSDate()
    let earliest = now.earlierDate(date)
    let latest = (earliest == now) ? date : now
    let components:NSDateComponents = calendar.components(unitFlags, fromDate: earliest, toDate: latest, options: nil)
    
    if (components.year >= 2) {
        return "\(components.year) years ago"
    } else if (components.year >= 1){
        if (numericDates){
            return "1 year ago"
        } else {
            return "Last year"
        }
    } else if (components.month >= 2) {
        return "\(components.month) months ago"
    } else if (components.month >= 1){
        if (numericDates){
            return "1 month ago"
        } else {
            return "Last month"
        }
    } else if (components.weekOfYear >= 2) {
        return "\(components.weekOfYear) weeks ago"
    } else if (components.weekOfYear >= 1){
        if (numericDates){
            return "1 week ago"
        } else {
            return "Last week"
        }
    } else if (components.day >= 2) {
        return "\(components.day) days ago"
    } else if (components.day >= 1){
        if (numericDates){
            return "1 day ago"
        } else {
            return "Yesterday"
        }
    } else if (components.hour >= 2) {
        return "\(components.hour) hours ago"
    } else if (components.hour >= 1){
        if (numericDates){
            return "1 hour ago"
        } else {
            return "An hour ago"
        }
    } else if (components.minute >= 2) {
        return "\(components.minute) minutes ago"
    } else if (components.minute >= 1){
        if (numericDates){
            return "1 minute ago"
        } else {
            return "A minute ago"
        }
    } else if (components.second >= 3) {
        return "\(components.second) seconds ago"
    } else {
        return "Just now"
    }
}