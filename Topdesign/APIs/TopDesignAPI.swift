//
//  TopDesignAPI.swift
//  Topdesign
//
//  Created by Griffith Chen on 6/16/15.
//  Copyright (c) 2015 Topdesign. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class TopDesignAPI {
    private let baseURL: String
    
    class var sharedInstance: TopDesignAPI {
        struct Static {
            static let instance = TopDesignAPI()
        }
        return Static.instance
    }
    
    init() {
        baseURL = Config().getValue("API_BASE_URL")
    }
    
    func getSession(success: (SwiftyJSON.JSON) -> Void, failure: (NSError) -> Void) {
        let apiURL = "\(baseURL)/user/session"
        
        Alamofire.request(.GET, apiURL)
            .responseJSON { (request, response, data, error) in
                if (error == nil) {
                    var JSONParse = data as! NSDictionary
                    var responseJSON = SwiftyJSON.JSON(JSONParse)
                    
                    success(responseJSON)
                }
                else {
                    failure(self.error(response!.statusCode))
                }
            }
    }
    
    func getListing(filter: String, page: Int, completion: (SwiftyJSON.JSON) -> Void) {
        let apiURL = "\(baseURL)/set/listing"
        
        Alamofire.request(.GET, apiURL, parameters: [ "page": page, "size": 20, "sorting": filter ])
            .responseJSON { (request, response, data, error) in
                if (error == nil) {
                    var JSONParse = data as! NSDictionary
                    var responseJSON = SwiftyJSON.JSON(JSONParse)
                    
                    completion(responseJSON)
                }
            }
    }
    
    func getListingByTag(tag: String, page: Int, completion: (SwiftyJSON.JSON) -> Void) {
        let apiURL = "\(baseURL)/set/tagged/\(tag)"
        
        Alamofire.request(.GET, apiURL, parameters: [ "page": page, "size": 20 ])
            .responseJSON { (request, response, data, error) in
                if (error == nil) {
                    var JSONParse = data as! NSDictionary
                    var responseJSON = SwiftyJSON.JSON(JSONParse)
                    
                    completion(responseJSON)
                }
            }
    }
    
    func getListingByEntityAndStateByUserId(userId: String, entity: String, state: String, completion: (SwiftyJSON.JSON) -> Void) {
        var apiURL: String = ""
        
        if entity == "user" {
            apiURL = "\(baseURL)/\(entity)/\(userId)/\(state)"
        }
        else {
            apiURL = "\(baseURL)/\(entity)/\(state)/\(userId)"
        }
        
        Alamofire.request(.GET, apiURL, parameters: [ "page": 1, "size": 20 ])
            .responseJSON { (request, response, data, error) in
                if (error == nil) {
                    var JSONParse = data as! NSDictionary
                    var responseJSON = SwiftyJSON.JSON(JSONParse)
                    
                    completion(responseJSON)
                }
            }
    }
    
    func getSetById(setId: String, completion: (SwiftyJSON.JSON) -> Void) {
        let apiURL = "\(baseURL)/set/\(setId)"
        
        Alamofire.request(.GET, apiURL)
            .responseJSON { (request, response, data, error) in
                if (error == nil) {
                    var JSONParse = data as! NSDictionary
                    var responseJSON = SwiftyJSON.JSON(JSONParse)
                    
                    completion(responseJSON)
                }
            }
    }
    
    func getUserById(userId: String, completion: (SwiftyJSON.JSON) -> Void) {
        let apiURL = "\(baseURL)/user/\(userId)"
        
        Alamofire.request(.GET, apiURL)
            .responseJSON { (request, response, data, error) in
                if (error == nil) {
                    var JSONParse = data as! NSDictionary
                    var responseJSON = SwiftyJSON.JSON(JSONParse)
                    
                    completion(responseJSON)
                }
            }
    }
    
    func getProductsByLabelId(setId: String, labelId: String, page: Int, completion: (SwiftyJSON.JSON) -> Void) {
        let apiURL = "\(baseURL)/product/listing"
        
        Alamofire.request(.GET, apiURL, parameters: [ "set_id": setId, "label_id": labelId, "page": page, "size": 20 ])
            .responseJSON { (request, response, data, error) in
                if (error == nil) {
                    var JSONParse = data as! NSDictionary
                    var responseJSON = SwiftyJSON.JSON(JSONParse)
                    
                    completion(responseJSON)
                }
            }
    }
   
    func search(term: String, type: String, completion: (SwiftyJSON.JSON) -> Void) {
        let apiURL = "\(baseURL)/hint"
        
        Alamofire.request(.GET, apiURL, parameters: [ "text": term, "doc_type": type ])
            .responseJSON { (request, response, data, error) in
                if (error == nil) {
                    var JSONParse = data as! NSDictionary
                    var responseJSON = SwiftyJSON.JSON(JSONParse)
                    
                    completion(responseJSON)
                }
            }
    }
    
    func loginWithFacebook(user: NSDictionary, success: (SwiftyJSON.JSON) -> Void, failure: (NSError) -> Void) {
        var params = Dictionary<String, String>()
        let userID = user.objectForKey("id") as! String
        
        params["social_id"] = userID
        params["email"] = user.objectForKey("email") as? String
        params["first_name"] = user.objectForKey("first_name") as? String
        params["last_name"] = user.objectForKey("last_name") as? String
        params["gender"] = user.objectForKey("gender") as? String
        params["avatar"] = "https://graph.facebook.com/\(userID)/picture"
        
        Alamofire.request(.POST, baseURL + "/user/login/facebook", parameters: params, encoding: .JSON)
            .responseJSON { (request, response, data, error) in
                if (error == nil) {
                    var JSONParse = data as! NSDictionary
                    var responseJSON = SwiftyJSON.JSON(JSONParse)
                    
                    success(responseJSON)
                }
                else {
                    failure(self.error(response!.statusCode))
                }
            }
    }
    
    func likeOrUnlikeById(entity: String, entityId: String, isLike: Bool, success: (SwiftyJSON.JSON) -> Void, failure: (NSError) -> Void) {
        let apiURL = "\(baseURL)/like/\(entity)/\(entityId)"
        let method = isLike ? Alamofire.Method.POST : Alamofire.Method.DELETE
        
        Alamofire.request(method, apiURL)
            .responseJSON { (request, response, data, error) in
                if (error == nil) {
                    var JSONParse = data as! NSDictionary
                    var responseJSON = SwiftyJSON.JSON(JSONParse)
                    
                    success(responseJSON)
                }
                else {
                    failure(self.error(response!.statusCode))
                }
            }
    }
    
    func followOrUnfollowByUserId(userId: String, isFollow: Bool, success: (SwiftyJSON.JSON) -> Void, failure: (NSError) -> Void) {
        let apiURL = "\(baseURL)/follow/\(userId)"
        let method = isFollow ? Alamofire.Method.POST : Alamofire.Method.DELETE
        
        Alamofire.request(method, apiURL)
            .responseJSON { (request, response, data, error) in
                if (error == nil) {
                    var JSONParse = data as! NSDictionary
                    var responseJSON = SwiftyJSON.JSON(JSONParse)
                    
                    success(responseJSON)
                }
                else {
                    failure(self.error(response!.statusCode))
                }
        }
    }

    func setUserAccount( params: [String: String], success: (SwiftyJSON.JSON) -> Void, failure: (NSError) -> Void) {
        let apiURL = "\(baseURL)/user/settings/update"
        Alamofire.request(.POST, apiURL, parameters: params, encoding: .JSON)
            .responseJSON { (request, response, data, error) in
                if (error == nil) {
                    var JSONParse = data as! NSDictionary
                    var responseJSON = SwiftyJSON.JSON(JSONParse)
                    success(responseJSON)
                }
                else {
                    failure(self.error(response!.statusCode))
                }
            }
    }
    
    func S3GenerateSignature(mimeType: String, success: (SwiftyJSON.JSON) -> Void, failure: (NSError) -> Void) {
        Alamofire.request(.GET, baseURL + "/sign_s3", parameters: ["mime_type" : mimeType])
            .responseJSON { (request, response, data, error) in
                if (error == nil) {
                    var JSONParse = data as! NSDictionary
                    var responseJSON = SwiftyJSON.JSON(JSONParse)
                    
                    success(responseJSON)
                }
                else {
                    failure(self.error(response!.statusCode))
                }
            }
    }
    
    func createSet(imageURL: String, tags: NSArray, description: String, success: (SwiftyJSON.JSON) -> Void, failure: (NSError) -> Void) {
        var params = Dictionary<String, AnyObject>()
        
        params["tags"] = tags
        params["image_url"] = imageURL
        params["image_type"] = "ext"
        params["description"] = description
        
        Alamofire.request(.POST, baseURL + "/set/add", parameters: params, encoding: .JSON)
            .responseJSON { (request, response, data, error) in
                if (error == nil) {
                    var JSONParse = data as! NSDictionary
                    var responseJSON = SwiftyJSON.JSON(JSONParse)
                    
                    success(responseJSON)
                }
                else {
                    failure(self.error(response!.statusCode))
                }
            }
    }
    
    func error(code: Int) -> NSError {
        let details = NSMutableDictionary()
        var description = ""
        
        switch code {
        case 400:
            description = "Bad request"
        default:
            description = "Unknown error"
        }
        
        details[NSLocalizedDescriptionKey] = description
        
        return NSError(domain: "TopDesignAPI", code: code, userInfo: details as [NSObject: AnyObject])
    }
}
