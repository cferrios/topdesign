//
//  LoginModalView.swift
//  Topdesign
//
//  Created by Griffith Chen on 7/1/15.
//  Copyright (c) 2015 Topdesign. All rights reserved.
//

import UIKit
import SwiftyJSON
import FBSDKLoginKit

class LoginViewController: UIViewController {
    
    let FBPermissions = ["public_profile", "email", "user_friends"]
    let FBUserFields = ["fields": "email, first_name, last_name, gender"]
    
    override func loadView() {
        super.loadView()
        
        self.view.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        
        setupContent()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.hidden = true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupContent() {
        let screen = UIScreen.mainScreen().bounds
        let scale = UIScreen.mainScreen().scale
        
        let height = CGFloat(220)
        let width = screen.width * 0.8
        let offsetX = screen.minX + (screen.width * 0.1)
        let offsetY = (screen.height / 2) - (height / 2)
        
        // Content view.
        let cv = UIView()
        cv.frame = CGRectMake(offsetX, offsetY, width, height)
        cv.backgroundColor = UIColor.whiteColor()
        
        // Close button.
        let closeButton = UIButton()
        let cbSize = 8 * scale
        closeButton.frame = CGRectMake(cv.frame.width - (8 + cbSize), 8, cbSize, cbSize)
        closeButton.setTitle("x", forState: .Normal)
        closeButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        closeButton.titleLabel?.font = UIFont.boldSystemFontOfSize(8 * scale)
        closeButton.addTarget(self, action: "close:", forControlEvents: .TouchUpInside)
        
        // Logo.
        let logo = UILabel()
        logo.frame = CGRectMake(0, 30, cv.frame.width, 20)
        logo.font = logo.font.fontWithSize(18)
        logo.text = "TopDesign.me"
        logo.numberOfLines = 0
        logo.textAlignment = .Center
        
        // Slogan.
        let tagline = UILabel()
        tagline.frame = CGRectMake(0, logo.frame.maxY + 20, cv.frame.width, 40)
        tagline.text = "Turn design inspirations\ninto action"
        tagline.font = tagline.font.fontWithSize(14)
        tagline.numberOfLines = 2
        tagline.textAlignment = .Center
        
        // Facebook button.
        let FBButton = UIButton()
        FBButton.frame = CGRectMake(10, tagline.frame.maxY + 30, cv.frame.width - 20, 45)
        FBButton.backgroundColor = UIColor(hex: 0x0E5395)
        FBButton.setTitle("Sign in with Facebook", forState: .Normal)
        FBButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        FBButton.titleLabel?.font = UIFont.boldSystemFontOfSize(16)
        FBButton.addTarget(self, action: "signInFacebook:", forControlEvents: .TouchUpInside)
        
        cv.addSubview(closeButton)
        cv.addSubview(logo)
        cv.addSubview(tagline)
        cv.addSubview(FBButton)
        
        self.view.addSubview(cv)
    }
    
    func close(sender: UIButton) {
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    func signInFacebook(sender: UIButton) {
        if FBSDKAccessToken.currentAccessToken() != nil {
            FBSDKGraphRequest(graphPath: "me", parameters: self.FBUserFields).startWithCompletionHandler({ (connection, result, error) -> Void in
                if error != nil {
                    FBSDKLoginManager().logOut()
                    self.signInFacebookFailure(nil)
                }
                else {
                    self.signInFacebookSuccess(result as! NSDictionary)
                }
            })
            
            return
        }
        
        FBSDKLoginManager().logInWithReadPermissions(
            self.FBPermissions,
            handler: { (result: FBSDKLoginManagerLoginResult!, error: NSError!) -> Void in
                if error != nil {
                    FBSDKLoginManager().logOut()
                    self.signInFacebookFailure(error)
                }
                else if result.isCancelled {
                    FBSDKLoginManager().logOut()
                    self.signInFacebookFailure(nil)
                }
                else {
                    var hasAllPermissions = true
                    let grantedPermissions = NSSet(set: result.grantedPermissions).allObjects.map({"\($0)"})
                    
                    for permission in self.FBPermissions {
                        if !contains(grantedPermissions, permission) {
                            hasAllPermissions = false
                            break
                        }
                    }
                    
                    if hasAllPermissions {
                        let FBToken = result.token.tokenString
                        let FBUserID = result.token.userID
                        
                        FBSDKGraphRequest(graphPath: "me", parameters: self.FBUserFields).startWithCompletionHandler({ (connection, result, error) -> Void in
                            if error != nil {
                                FBSDKLoginManager().logOut()
                                self.signInFacebookFailure(nil)
                            }
                            else {
                                self.signInFacebookSuccess(result as! NSDictionary)
                            }
                        })
                    }
                    else {
                        self.signInFacebookFailure(nil)
                    }
                }
            }
        )
    }
    
    func signInFacebookFailure(error: NSError?) {
        var message = "Unable to login to Facebook"
        
        if error != nil {
            message += " (\(error!.code))"
        }
        
        self.error("Login Error", message: message)
    }
    
    func signInFacebookSuccess(result: NSDictionary!) {
        TopDesignAPI.sharedInstance.loginWithFacebook(
            result,
            success: { (data) -> Void in
                Session.sharedInstance.sync(data)
                self.dismissViewControllerAnimated(false, completion: nil)
            },
            failure: { (error) -> Void in
                self.error("Login Error", message: error.localizedDescription)
            }
        )
    }
    
    func error(title: String, message: String) {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alertView.addAction(UIAlertAction(title: "Dismiss", style: .Default, handler: nil))
        
        self.dismissViewControllerAnimated(false, completion: {
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let rootVC = appDelegate.window!.rootViewController as! ApplicationTabBarController
            
            rootVC.selectedViewController?.presentViewController(alertView, animated: true, completion: nil)
        })
    }
}