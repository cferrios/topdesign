//
//  BrowseViewController.swift
//  Topdesign
//
//  Created by Griffith Chen on 6/3/15.
//  Copyright (c) 2015 Topdesign. All rights reserved.
//

import UIKit
import Haneke
import SwiftyJSON

class BrowseViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, CHTCollectionViewDelegateWaterfallLayout {
    
    let filterItems = ["Newest", "Trending", "Updated"]
    var filterPages = [1, 1, 1]
    
    var collectionView: UICollectionView!
    var filterView: UISegmentedControl!
    var refreshControl: UIRefreshControl!
    
    var model: [SwiftyJSON.JSON] = []
    var selectedFilter: Int = 0
    
    var lastScrollOffsetY: CGFloat = 0
    var scrollDirection: ScrollDirection = .None
    var isSubmitting: Bool! = false

    override func loadView() {
        super.loadView()
        
        self.view.backgroundColor = UIColor.whiteColor()
        
        setupSegmentedControlView()
        setupCollectionView()
        setupRefresh()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        toggleApplicationTabBar(true, false)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupRefresh() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: "refresh:", forControlEvents: .ValueChanged)
        
        self.collectionView.addSubview(self.refreshControl)
    }
    
    func setupSegmentedControlView() {
        let screen = UIScreen.mainScreen().bounds
        self.filterView = UISegmentedControl(items: self.filterItems)
        
        self.filterView.selectedSegmentIndex = 0
        self.filterView.frame = CGRectMake(screen.minX + 10, screen.minY + 30, screen.width - 20, screen.height * 0.05)
        self.filterView.addTarget(self, action: "selectFilter:", forControlEvents: .ValueChanged)
        
        self.view.addSubview(self.filterView)
        self.loadFeed(self.selectedFilter, page: 1)
    }
    
    func setupCollectionView() {
        let screen = UIScreen.mainScreen().bounds
        let frame = CGRectMake(screen.minX + 10, screen.minY + self.filterView.frame.height + 45, screen.width - 20, screen.height * 0.95)  
        let layout = CHTCollectionViewWaterfallLayout()
        let viewNib = UINib(nibName: "ImageUICollectionViewCell", bundle: nil)
        
        layout.minimumColumnSpacing = 5.0
        layout.minimumInteritemSpacing = 5.0
        
        self.collectionView = UICollectionView(frame: frame, collectionViewLayout: layout)
        self.collectionView.registerNib(viewNib, forCellWithReuseIdentifier: "cell")
        self.collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
        self.collectionView.backgroundColor = UIColor.whiteColor()
        self.collectionView.showsVerticalScrollIndicator = false
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        self.collectionView.autoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth
        self.collectionView.alwaysBounceVertical = false
        
        self.view.addSubview(self.collectionView)
    }
    
    func loadFeed(filterIndex: Int, page: Int) {
        if self.isSubmitting == true {
            return
        }
        
        let filter = self.filterItems[filterIndex].lowercaseString
        self.isSubmitting = true
        
        TopDesignAPI.sharedInstance.getListing(filter, page: page) { (data) -> Void in
            self.renderFeed(data)
            
            self.filterPages[filterIndex] = page
            
            self.isSubmitting = false
            self.refreshControl.endRefreshing()
        }
    }
 
    func renderFeed(data: SwiftyJSON.JSON) {
        let hits = data["hits"].arrayValue as [SwiftyJSON.JSON]
        let total = data["total"].intValue
        
        var index = self.model.count
        var indexPaths = [NSIndexPath]()
        
        for hit in hits {
            let indexPath = NSIndexPath(forItem: index++, inSection: 0)
            indexPaths.append(indexPath)
            
            self.model.append(hit)
        }
        
        self.collectionView.performBatchUpdates({
                self.collectionView.insertItemsAtIndexPaths(indexPaths)
            }, completion: nil)
    }
   
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! ImageUICollectionViewCell
        
        let data = model[indexPath.row]
        let url = NSURL(string: data["_source"]["image"]["url"].stringValue)
        
        cell.image.hnk_setImageFromURL(url!, placeholder: UIImage.imageWithColor(UIColor.lightGrayColor()))
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout!, sizeForItemAtIndexPath indexPath: NSIndexPath!) -> CGSize {
        let data = model[indexPath.row]
        let width = data["_source"]["image"]["width"].intValue
        let height = data["_source"]["image"]["height"].intValue
        let imageSize = CGSize(width: width, height: height)
        
        return imageSize
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if self.isSubmitting == true {
            return
        }
        
        let data = model[indexPath.row]
        let cell = collectionView.cellForItemAtIndexPath(indexPath)!
        
        let setViewController = SetViewController(nibName: nil, bundle: nil)
        let setId = data["_id"].stringValue
        
        self.isSubmitting = true
        
        TopDesignAPI.sharedInstance.getSetById(setId) { (data) -> Void in
            setViewController.render(setId, model: data)
            self.navigationController?.pushViewController(setViewController, animated: true)
            
            self.isSubmitting = false
        }
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let screen = UIScreen.mainScreen().bounds
        let height = scrollView.contentSize.height - scrollView.frame.size.height - (screen.height / 4)
    
        if scrollView.contentOffset.y <= 0 {
            toggleApplicationTabBar(true, true)
        }
        else {
            self.scrollDirection = (scrollView.contentOffset.y < self.lastScrollOffsetY) ? .Up : .Down
            self.lastScrollOffsetY = scrollView.contentOffset.y
            
            toggleApplicationTabBar(self.scrollDirection == .Up, true)
        }
        
        if scrollView.contentOffset.y >= height {
            loadFeed(self.selectedFilter, page: (self.filterPages[self.selectedFilter] + 1))
        }
    }
    
    func selectFilter(sender: UISegmentedControl) {
        self.model = []
        self.collectionView.setContentOffset(CGPointZero, animated: false)
        self.collectionView.reloadData()
        
        loadFeed(sender.selectedSegmentIndex, page: 1)
    }
    
    func refresh(sender: UIRefreshControl!) {
        self.model = []
        self.collectionView.reloadData()
        
        loadFeed(self.selectedFilter, page: 1)
    }
}