//
//  AddSetViewController.swift
//  Topdesign
//
//  Created by Griffith Chen on 8/3/15.
//  Copyright (c) 2015 Topdesign. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import VENTokenField
import SwiftOverlays

class AddSetViewController: UIViewController, UINavigationBarDelegate, UITextViewDelegate, UITableViewDelegate, UITableViewDataSource, VENTokenFieldDelegate, VENTokenFieldDataSource {
    let descriptionPlaceholderText = "e.g. Comes in tons of colors and sizes (optional)"
    
    var searchDebounced: (() -> ())!
    var searchText: String!
    
    var mainView: UIScrollView!
    var photoView: UIImageView!
    var tagTextField: VENTokenField!
    var tagSuggestionView: UITableView!
    var tagSuggestionViewHeightConstraint: NSLayoutConstraint!
    var descTextView: UITextView!
    
    var tags: [SwiftyJSON.JSON] = []
    var tokens: [String] = []
    var scrollOffset: CGPoint!
    
    var isSubmitting: Bool = false
    
    override func loadView() {
        super.loadView()
        
        let screen = UIScreen.mainScreen().bounds
        let tapRecognizer = UITapGestureRecognizer(target: self, action: Selector("hideKeyboard"))
        tapRecognizer.cancelsTouchesInView = false
        
        self.view.backgroundColor = UIColor.whiteColor()
        self.view.addGestureRecognizer(tapRecognizer)
        
        self.mainView = UIScrollView(frame: CGRectMake(screen.minX, screen.minY, screen.width, screen.height))
        self.view.addSubview(self.mainView)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillAppear"), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillDisappear"), name: UIKeyboardWillHideNotification, object: nil)
        
        setupNavigation()
        setupForm()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupPhoto(imageView: UIImageView) {
        self.photoView.image = imageView.image!
    }
    
    func setupNavigation() {
        let screen = UIScreen.mainScreen().bounds
        
        let navBar = UINavigationBar(frame: CGRectMake(screen.minX, screen.minY + 20, screen.width, 44))
        navBar.barTintColor = UIColor.whiteColor()
        navBar.delegate = self
        
        let navBarItem = UINavigationItem()
        navBarItem.title = "Add details"
        
        let closeButton = UIBarButtonItem(image: UIImage(named: "CloseIcon"), style: .Plain, target: self, action: "close:")
        let confirmButton = UIBarButtonItem(image: UIImage(named: "ConfirmIcon"), style: .Plain, target: self, action: "confirm:")
        
        navBarItem.leftBarButtonItem = closeButton
        navBarItem.rightBarButtonItem = confirmButton
        
        navBar.items = [navBarItem]
        self.mainView.addSubview(navBar)
    }
    
    func setupForm() {
        let screen = UIScreen.mainScreen().bounds
        let headerHeight = CGFloat(50)
        
        self.photoView = UIImageView(frame: CGRectMake(screen.minX, screen.minY + 64, screen.width, screen.height / 4))
        self.photoView.image = nil
        self.photoView.contentMode = .ScaleAspectFit
        self.photoView.backgroundColor = UIColor.blackColor()
        
        let tagHeaderView = UIView(frame: CGRectMake(screen.minX, self.photoView.frame.maxY, screen.width, headerHeight))
        tagHeaderView.backgroundColor = UIColor(hex: 0xD4D4D4)
        
        let tagHeaderViewLabel = UILabel(frame: CGRectMake(10, 0, tagHeaderView.frame.width, tagHeaderView.frame.height))
        tagHeaderViewLabel.text = "ADD HASHTAGS"
        tagHeaderViewLabel.font = UIFont.boldSystemFontOfSize(18)
        tagHeaderView.addSubview(tagHeaderViewLabel)
        
        self.tagTextField = VENTokenField(frame: CGRectMake(10, tagHeaderView.frame.maxY + 10, screen.width - 20, 0))
        self.tagTextField.toLabelText = nil
        
        self.tagTextField.layer.borderColor = UIColor.blackColor().CGColor
        self.tagTextField.layer.borderWidth = 1
        self.tagTextField.layer.cornerRadius = 6
        
        self.tagTextField.delegate = self
        self.tagTextField.dataSource = self
        
        let descHeaderView = UIView(frame: CGRectMake(screen.minX, tagTextField.frame.maxY + 10, screen.width, headerHeight))
        descHeaderView.setTranslatesAutoresizingMaskIntoConstraints(false)
        descHeaderView.backgroundColor = UIColor(hex: 0xD4D4D4)
        
        let descHeaderViewLabel = UILabel(frame: CGRectMake(10, 0, tagHeaderView.frame.width, descHeaderView.frame.height))
        descHeaderViewLabel.text = "ENTER DETAILS"
        descHeaderViewLabel.font = UIFont.boldSystemFontOfSize(18)
        descHeaderView.addSubview(descHeaderViewLabel)
        
        self.descTextView = UITextView(frame: CGRectMake(10, descHeaderView.frame.maxY + 10, screen.width - 20, screen.height / 5))
        self.descTextView.setTranslatesAutoresizingMaskIntoConstraints(false)
        self.descTextView.delegate = self
       
        self.descTextView.layer.borderColor = UIColor.blackColor().CGColor
        self.descTextView.layer.borderWidth = 1
        self.descTextView.layer.cornerRadius = 6
        
        self.descTextView.text = self.descriptionPlaceholderText
        self.descTextView.textColor = UIColor.lightGrayColor()
        self.descTextView.font = UIFont.systemFontOfSize(14)
        self.descTextView.selectedTextRange = self.descTextView.textRangeFromPosition(self.descTextView.beginningOfDocument, toPosition: self.descTextView.beginningOfDocument)
        
        self.tagSuggestionView = UITableView(frame: CGRectMake(screen.minX, tagTextField.frame.maxY, screen.width, screen.height - tagTextField.frame.maxY))
        self.tagSuggestionView.setTranslatesAutoresizingMaskIntoConstraints(false)
        self.tagSuggestionView.alwaysBounceVertical = false
        self.tagSuggestionView.hidden = true
        
        self.tagSuggestionView.delegate = self
        self.tagSuggestionView.dataSource = self
        
        self.mainView.addSubview(self.photoView)
        self.mainView.addSubview(tagHeaderView)
        self.mainView.addSubview(self.tagTextField)
        self.mainView.addSubview(descHeaderView)
        self.mainView.addSubview(self.descTextView)
        self.mainView.addSubview(self.tagSuggestionView)
        
        self.mainView.addConstraint(NSLayoutConstraint(item: descHeaderView, attribute: .Width, relatedBy: .Equal, toItem: self.mainView, attribute: .Width, multiplier: 1, constant: 0))
        self.mainView.addConstraint(NSLayoutConstraint(item: descHeaderView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: headerHeight))
        self.mainView.addConstraint(NSLayoutConstraint(item: descHeaderView, attribute: .Top, relatedBy: .Equal, toItem: self.tagTextField, attribute: .Bottom, multiplier: 1, constant: 10))
        self.mainView.addConstraint(NSLayoutConstraint(item: descHeaderView, attribute: .CenterX, relatedBy: .Equal, toItem: self.tagTextField, attribute: .CenterX, multiplier: 1, constant: 0))
        
        self.mainView.addConstraint(NSLayoutConstraint(item: self.descTextView, attribute: .Width, relatedBy: .Equal, toItem: self.mainView, attribute: .Width, multiplier: 1, constant: -20))
        self.mainView.addConstraint(NSLayoutConstraint(item: self.descTextView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: screen.height / 5))
        self.mainView.addConstraint(NSLayoutConstraint(item: self.descTextView, attribute: .Top, relatedBy: .Equal, toItem: descHeaderView, attribute: .Bottom, multiplier: 1, constant: 10))
        self.mainView.addConstraint(NSLayoutConstraint(item: self.descTextView, attribute: .CenterX, relatedBy: .Equal, toItem: descHeaderView, attribute: .CenterX, multiplier: 1, constant: 0))
        
        self.mainView.addConstraint(NSLayoutConstraint(item: self.tagSuggestionView, attribute: .Width, relatedBy: .Equal, toItem: self.mainView, attribute: .Width, multiplier: 1, constant: 0))
        self.mainView.addConstraint(NSLayoutConstraint(item: self.tagSuggestionView, attribute: .Top, relatedBy: .Equal, toItem: self.tagTextField, attribute: .Bottom, multiplier: 1, constant: 0))
        self.mainView.addConstraint(NSLayoutConstraint(item: self.tagSuggestionView, attribute: .CenterX, relatedBy: .Equal, toItem: descHeaderView, attribute: .CenterX, multiplier: 1, constant: 0))
        
        self.tagSuggestionViewHeightConstraint = NSLayoutConstraint(item: self.tagSuggestionView, attribute: .Height, relatedBy: .Equal, toItem: self.mainView, attribute: .Height, multiplier: 1, constant: 0)
        self.mainView.addConstraint(self.tagSuggestionViewHeightConstraint)
        
        self.searchDebounced = debounce(NSTimeInterval(0.25), queue: dispatch_get_main_queue(), self.search)
    }
    
    func search() {
        if count(self.searchText) == 0 {
            self.tags = []
            self.tagSuggestionView.hidden = true
            self.tagSuggestionView.reloadData()
            
            return
        }
        
        TopDesignAPI.sharedInstance.search(self.searchText, type: "tag") { (data) -> Void in
            self.tags = data["@tags"].arrayValue as [SwiftyJSON.JSON]
            
            if self.tags.count == 0 {
                self.tags.append(SwiftyJSON.JSON(self.searchText))
            }
            
            self.tagSuggestionViewHeightConstraint.constant = -self.tagTextField.frame.maxY
            
            self.tagSuggestionView.hidden = false
            self.tagSuggestionView.reloadData()
        }
    }
    
    func close(sender: UIBarButtonItem!) {
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    func confirm(sender: UIBarButtonItem!) {
        if self.isSubmitting == true {
            return
        }
        
        self.isSubmitting = true
        SwiftOverlays.showBlockingWaitOverlayWithText("Submitting")
        
        TopDesignAPI.sharedInstance.S3GenerateSignature("image/jpeg",
            success: { (data) -> Void in
                let url: String = data["url"].stringValue
                let request: String = data["signed_request"].stringValue
                
                let photo: NSData = UIImageJPEGRepresentation(self.photoView.image, 1.0)
                
                var putRequest: NSMutableURLRequest = NSMutableURLRequest(URL: NSURL(string: request)!)
                putRequest.HTTPMethod = Alamofire.Method.PUT.rawValue
                putRequest.setValue("public-read", forHTTPHeaderField: "x-amz-acl")
                putRequest.setValue("image/jpeg", forHTTPHeaderField: "Content-Type")
                
                Alamofire.Manager.sharedInstance.upload(putRequest, data: photo)
                    .responseJSON { (S3Request, S3Response, S3Data, S3Error) in
                        if S3Error == nil && S3Response!.statusCode == 200 {
                            self.submit(url)
                            return
                        }
                        
                        self.error("Error", message: "Could not post set. Please try again. (S3 Connection Failure)")
                    }
            },
            failure: { (error) -> Void in
                self.error("Error", message: error.localizedDescription)
            })
    }
    
    func submit(imageURL: String) {
        var description: String = self.descTextView.text
        if self.descTextView.textColor == UIColor.lightGrayColor() && count(self.descTextView.text) > 0 {
            description = ""
        }
       
        TopDesignAPI.sharedInstance.createSet(imageURL, tags: self.tokens, description: description,
            success: { (data) -> Void in
                let setViewController = SetViewController(nibName: nil, bundle: nil)
                let setId = data["_id"].stringValue
                
                TopDesignAPI.sharedInstance.getSetById(setId) { (data) -> Void in
                    setViewController.render(setId, model: data)
                    self.dismissViewControllerAnimated(false, completion: nil)
                    
                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    let tabBar = appDelegate.window!.rootViewController as! ApplicationTabBarController
                    
                    tabBar.selectedIndex = 0
                    tabBar.childViewControllers[0].pushViewController(setViewController, animated: true)
                    
                    self.isSubmitting = false
                    SwiftOverlays.removeAllBlockingOverlays()
                }
            },
            failure: { (error) -> Void in
                self.error("Error", message: error.localizedDescription)
                
                self.isSubmitting = false
                SwiftOverlays.removeAllBlockingOverlays()
            })
    }
    
    func keyboardWillAppear() {
        var rect: CGRect = self.tagTextField.bounds
        rect = self.tagTextField.convertRect(rect, toView: self.mainView)
    
        var offset: CGPoint = rect.origin
        offset.x = 0
        offset.y -= 80
        
        self.mainView.setContentOffset(offset, animated: true)
    }
    
    func keyboardWillDisappear() {
        self.mainView.setContentOffset(CGPointZero, animated: true)
    }
    
    func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text as NSString
        let updatedText = currentText.stringByReplacingCharactersInRange(range, withString: text)
        
        if count(updatedText) == 0 {
            textView.text = self.descriptionPlaceholderText
            textView.textColor = UIColor.lightGrayColor()
            return false
        }
        
        if textView.textColor == UIColor.lightGrayColor() && count(text) > 0 {
            textView.text = nil
            textView.textColor = UIColor.blackColor()
        }
       
        return true
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tags.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let data = self.tags[indexPath.row]
        let cell = UITableViewCell(style: .Default, reuseIdentifier: nil)
        
        cell.textLabel?.text = data.stringValue
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let data = self.tags[indexPath.row]
        let matches = self.tokens.filter() { return ($0 as String) == data.stringValue }
        
        if matches.count == 0 {
            self.scrollOffset = self.mainView.contentOffset
            
            self.tokens.append(data.stringValue)
            self.tagTextField.reloadData()
            
            self.mainView.setContentOffset(self.scrollOffset, animated: false)
        }
        
        self.tags = []
        self.tagSuggestionView.hidden = true
        self.tagSuggestionView.reloadData()
    }
    
    func tokenField(tokenField: VENTokenField!, didChangeText text: String!) {
        self.searchText = text
        self.searchDebounced()
    }
    
    func tokenField(tokenField: VENTokenField!, titleForTokenAtIndex index: UInt) -> String! {
        return self.tokens[Int(index)]
    }
    
    func tokenField(tokenField: VENTokenField!, didDeleteTokenAtIndex index: UInt) {
        self.tokens.removeAtIndex(Int(index))
        self.tagTextField.reloadData()
    }
    
    func numberOfTokensInTokenField(tokenField: VENTokenField!) -> UInt {
        return UInt(self.tokens.count)
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        var rect: CGRect = textView.bounds
        rect = textView.convertRect(rect, toView: self.mainView)
    
        var offset: CGPoint = rect.origin
        offset.x = 0
        offset.y -= 80
        
        self.mainView.setContentOffset(offset, animated: true)
    }
    
    func error(title: String, message: String) {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alertView.addAction(UIAlertAction(title: "Dismiss", style: .Default, handler: nil))
        
        self.presentViewController(alertView, animated: true, completion: nil)
    }
}