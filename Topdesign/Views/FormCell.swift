//
//  FormBaseCell.swift
//  SwiftForms
//
//  Created by Miguel Angel Ortuno on 20/08/14.
//  Copyright (c) 2014 Miguel Angel Ortuño. All rights reserved.
//

import UIKit
import SwiftForms

public class FormCell: FormValueCell {


    /// MARK: FormBaseCell
    
    public override func update() {
        super.update()
        
        titleLabel.text = rowDescriptor.title
        
        var title: String!
        
        if let selectedValues = rowDescriptor.value as? NSArray { // multiple values
            
            let indexedSelectedValues = NSSet(array: selectedValues as [AnyObject])
            
            if let options = rowDescriptor.configuration[FormRowDescriptor.Configuration.Options] as? NSArray {
                for optionValue in options {
                    if indexedSelectedValues.containsObject(optionValue) {
                        let optionTitle = rowDescriptor.titleForOptionValue(optionValue as! NSObject)
                        if title != nil {
                            title = title + ", \(optionTitle)"
                        }
                        else {
                            title = optionTitle
                        }
                    }
                }
            }
        }
        else if let selectedValue = rowDescriptor.value { // single value
            title = rowDescriptor.titleForOptionValue(selectedValue)
        }
        
        if title != nil && count(title) > 0 {
            valueLabel.text = title
            valueLabel.textColor = UIColor.blackColor()
        }
        else {
            valueLabel.text = rowDescriptor.configuration[FormRowDescriptor.Configuration.Placeholder] as? String
            valueLabel.textColor = UIColor.lightGrayColor()
        }
    }
    
    public override class func formViewController(formViewController: FormViewController, didSelectRow selectedRow: FormBaseCell) {
 
            formViewController.view.endEditing(true)
            
            var selectorClass: UIViewController.Type!

         
            selectorClass = FormViewController.self
            

        let form = FormDescriptor()
        form.title = "Account"

        
        // Define first section
        let section1 = FormSectionDescriptor()
        
        var row: FormRowDescriptor! = FormRowDescriptor(tag: "name", rowType: .Email, title: "Email")
        section1.addRow(row)
        
        row = FormRowDescriptor(tag: "pass", rowType: .Password, title: "Password")
        section1.addRow(row)
        
        
        form.addSection(section1)
        
        var profileEditViewController: FormViewController!
        profileEditViewController = FormViewController()
        profileEditViewController.form=form
        formViewController.navigationController?.pushViewController(profileEditViewController, animated: true)
        
        
    }
    

}
