//
//  AccountSettingViewController.swift
//  Topdesign
//
//  Created by Hill Liu on 8/6/15.
//  Copyright (c) 2015 Topdesign. All rights reserved.
//

import UIKit
import SwiftForms

class AccountSettingViewController: FormViewController {
    init() {
        super.init(style: .Grouped)
        self.loadForm()
    }
    
    internal override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init(coder aDecoder: NSCoder?) {
        super.init(coder: aDecoder!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Submit", style: .Plain, target: self, action: "submit:")
    }
    
    func submit(_: UIBarButtonItem!) {
    
    }
    
    private func loadForm() {
        let form = FormDescriptor()
        
        form.title = "Account"
        
        // Define first section
        let section1 = FormSectionDescriptor()
        
        var row: FormRowDescriptor!
        var str: String!
        row = FormRowDescriptor(tag: "enable", rowType: .BooleanSwitch, title: "Notifications")
        section1.addRow(row)
        
        form.addSection(section1)
        self.form = form
    }
}
