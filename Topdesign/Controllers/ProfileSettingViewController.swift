//
//  ProfileSettingViewController.swift
//  Topdesign
//
//  Created by Hill Liu on 8/5/15.
//  Copyright (c) 2015 Topdesign. All rights reserved.
//


import UIKit
import SwiftForms

class ProfileSettingViewController: FormViewController {

    struct Static {
        static let firstName = "firstName"
        static let lastName = "lastName"
        static let userName = "userName"
        static let about = "about"
        static let website = "website"
        static let location = "location"
    }

    struct DbField {
        static let firstName = "first_name"
        static let lastName = "last_name"
        static let userName = "user_name"
        static let about = "about"
        static let website = "website"
        static let location = "location"
    }
    
    init() {
        super.init(style: .Grouped)
        self.loadForm()
    }

    internal override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init(coder aDecoder: NSCoder?) {
        super.init(coder: aDecoder!)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Submit", style: .Plain, target: self, action: "submit:")
    }
   
    func submit(_: UIBarButtonItem!) {
        let message = self.form.formValues().description
        
        let alert: UIAlertView = UIAlertView(title: "Form output", message: message, delegate: nil, cancelButtonTitle: "OK")
        alert.show()
        var formValue: NSDictionary = self.form.formValues() as NSDictionary
        var data: [String: String] = [String: String]()
        data[DbField.firstName]=formValue.objectForKey(Static.firstName) as? String
        data[DbField.lastName]=formValue.objectForKey(Static.lastName) as? String
        data[DbField.userName]=formValue.objectForKey(Static.userName) as? String
        data[DbField.about]=formValue.objectForKey(Static.about) as? String
        data[DbField.website]=formValue.objectForKey(Static.website) as? String
        data[DbField.location]=formValue.objectForKey(Static.location) as? String
        
        
        //["about": self.form.valueForKey("")]
        TopDesignAPI.sharedInstance.setUserAccount(
            data,
            success: { (data) -> Void in
                var str: String!
                str = Session.sharedInstance.user?.userID
                println(str)
                TopDesignAPI.sharedInstance.getUserById(str) { (user) -> Void in
                    println(user)
                    Session.sharedInstance.sync(user["@user"])
                }
                
                println("aaa")
            },
            failure: { (error) -> Void in
                println("bbb")
            }
        );
    }
    
    private func loadForm() {
        let form = FormDescriptor()
        
        form.title = "Edit Profile"
        
        // Define first section
        let section1 = FormSectionDescriptor()
        
        var row: FormRowDescriptor!
        var str: String!
        
        str = Session.sharedInstance.user?.firstName;
        row = FormRowDescriptor(tag: Static.firstName, rowType: .Name, title: "First Name")
        row.configuration[FormRowDescriptor.Configuration.CellConfiguration] = ["textField.placeholder" : "e.g. Miguel Ángel", "textField.textAlignment" : NSTextAlignment.Right.rawValue, "textField.text": str ]
        section1.addRow(row)
        
        str = Session.sharedInstance.user?.lastName;
        row = FormRowDescriptor(tag: Static.lastName, rowType: .Name, title: "Last Name")
        row.configuration[FormRowDescriptor.Configuration.CellConfiguration] = ["textField.placeholder" : "e.g. Ortuño", "textField.textAlignment" : NSTextAlignment.Right.rawValue, "textField.text": str]
        section1.addRow(row)        

        str = Session.sharedInstance.user?.userName;
        row = FormRowDescriptor(tag: Static.userName, rowType: .Name, title: "User Name")
        row.configuration[FormRowDescriptor.Configuration.CellConfiguration] = ["textField.placeholder" : "e.g. Ortuño", "textField.textAlignment" : NSTextAlignment.Right.rawValue, "textField.text": str]
        section1.addRow(row)        
        
        str = Session.sharedInstance.user?.about;
        row = FormRowDescriptor(tag: Static.about, rowType: .MultilineText, title: "About You")
        row.configuration[FormRowDescriptor.Configuration.CellConfiguration] = ["textField.text": str]
        section1.addRow(row)

        str = Session.sharedInstance.user?.website;
        row = FormRowDescriptor(tag: Static.website, rowType: .URL, title: "Web Site")
        row.configuration[FormRowDescriptor.Configuration.CellConfiguration] = ["textField.placeholder" : "e.g. gethooksapp.com", "textField.textAlignment" : NSTextAlignment.Right.rawValue, "textField.text": str]
        section1.addRow(row)

        str = Session.sharedInstance.user?.location;
        row = FormRowDescriptor(tag: Static.location, rowType: .Text, title: "Location")
        row.configuration[FormRowDescriptor.Configuration.CellConfiguration] = ["textField.placeholder" : "e.g. usa", "textField.textAlignment" : NSTextAlignment.Right.rawValue, "textField.text": str]
        section1.addRow(row)
        form.addSection(section1)
        self.form = form
    }

}
