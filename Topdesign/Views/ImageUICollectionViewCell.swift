//
//  ImageUICollectionViewCell.swift
//  Topdesign
//
//  Created by Griffith Chen on 6/11/15.
//  Copyright (c) 2015 Topdesign. All rights reserved.
//

import UIKit

class ImageUICollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}