# Carthage dependencies
* Alamofire (https://github.com/Alamofire/Alamofire)
* Haneke (https://github.com/Haneke/HanekeSwift)
* SwiftyJSON (https://github.com/SwiftyJSON/SwiftyJSON)

# Other depedencies
* Facebook iOS SDK
..* FBSDKCoreKit.framework
..* FBSDKLoginKit.framework