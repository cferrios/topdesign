//
//  AppConfig.swift
//  Topdesign
//
//  Created by Griffith Chen on 6/6/15.
//  Copyright (c) 2015 Topdesign. All rights reserved.
//

import Foundation

class Config: NSObject {
    static var plist: NSDictionary?
    
    override init() {
        super.init()
        
        if (Config.plist == nil) {
            let filePath = NSBundle.mainBundle().pathForResource("Topdesign", ofType: "plist")
            Config.plist = NSDictionary(contentsOfFile: filePath!)
        }
    }
    
    func getValue(keyname: String) -> String {
        let value: String = Config.plist?.objectForKey(keyname) as! String
        return value
    }
}